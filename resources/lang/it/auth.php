<?php

return [
    'failed' => 'Accesso fallito. Non esiste alcun account con le credenziali inserite.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'invalid_token' => 'Sembra che tu abbia aperto un link di reimpostazione della password non valido.',
];
