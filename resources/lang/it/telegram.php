<?php

return [
    'already_sync_subject' => 'Il tuo account è già stato accoppiato.',
    'already_sync_line1' => 'Questo numero è già stato collegato con l\'utente con indirizzo email :email.',
    'already_sync_line2' => 'Per rimuovere il collegamento invia',
    'already_sync_line3' => '`/stop conferma`',

    'invalid_token_subject' => 'Problema di sincronizzazione',
    'invalid_token_line1' => 'Per poter collegare questo numero al tuo account di :title devi effettuare la procedura guidata sull\'applicazione',
    'invalid_token_action' => 'Sincronizza numero',

    'sync_success_subject' => 'Account sincronizzato',
    'sync_success_line1' => 'Il tuo numero è stato correttamente sincronizzato con l\'account con email :email.',
    'sync_success_line2' => 'Se ti sei sbagliato e vuoi rimuovere il collegamento invia',
    'sync_success_line3' => '`/stop conferma`',
];
