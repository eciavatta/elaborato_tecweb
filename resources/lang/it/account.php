<?php

return [
    'wrong_password' => 'La password che hai inserito non è corretta',
    'delete_command' => 'elimina questo account',
    'wrong_command' => 'Verifica fallita. Riprova',
    'update_success' => 'Il tuo account è stato aggiornato con successo!'
];
