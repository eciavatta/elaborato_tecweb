<?php

return [
    'user_joined_subject' => 'Benvenuto su :title',
    'user_joined_greeting' => 'Ciao :name',
    'user_joined_line1' => 'Benvenuto su :title!',
    'user_joined_line2' => 'Ora puoi ordinare quello che vuoi, quando vuoi.',
    'user_joined_line3' => 'Ti aspettiamo!',
    'user_joined_action' => 'Sfoglia listino',
    'order_state_subject' => 'Stato dell\'ordine',
    'order_state_greeting' => 'Ciao :name',
    'order_state_line1' => 'Il tuo ordine è nello stato di :state',
    'order_state_wait_1' => 'Ci vorrà ancora qualche minuto.',
    'order_state_wait_2' => 'Tieniti pronto stiamo arrivando.',
    'order_state_wait_3' => 'Buon Pranzo!!',
    'order_state_action' => 'Sfoglia listino',
    'order_created_subject' => 'Ordine effettuato',
    'order_created_greeting' => 'Stiamo preparando il tuo ordine',
    'order_created_line1_delivery' => 'Un fattorino arriverà a portarti i tuoi articoli alle :time.',
    'order_created_line1_pickup' => 'I tuoi articoli saranno pronti per le ore :time.',
    'order_created_line2' => 'Di seguito sono elencati gli articoli acquistati:',
    'order_created_line3' => 'Totale (costi consegna inclusi): &euro; :price',
    'order_created_line4' => 'Ti invieremo una nuova notifica quando gli articoli saranno pronti!',
];
