import $ from 'jquery'

class EditMenu {

  static exec(path, dish) {
    const ctrl = EditMenu.inputController(dish)
    if (ctrl) {
      EditMenu.message(!ctrl, 'Non sono stati inseriti i dati correttamente')
    } else {
      $.post(path, dish, (data) => {
        if (data === '1') {
          EditMenu.message(true, 'Operazione completata')
          location.reload()
        } else {
          EditMenu.message(false, 'C\'é stato qualche problema nel server .. Straaaano ')
        }
      })
    }
  }

  static message(type, m) {
    $('.message').slideUp('slow')
    if (type) {
      $('.message').removeClass('alert-danger')
      $('.message').addClass('alert-success')
    } else {
      $('.message').addClass('alert-danger')
      $('.message').removeClass('alert-success')
    }
    $('.message').text(m)
    $('.message').slideDown('slow')
  }

  static inputController(data) {
    let x
    let flaggetto = false
    for (x in data) {
      if (data[x] === '') {
        flaggetto = !flaggetto
      }
    }
    return flaggetto
  }

  static newDishClick() {
    const dish = {
      name: $(this).parentsUntil('.form-group').parent().children('.dish').val(),
      price: $(this).parentsUntil('.form-group').parent().children('.price').val(),
      categoryId: $(this).parentsUntil('.editForm').parent().attr('category'),
      description: $(this).parentsUntil('.form-group').parent().children('.description').val()
    }
    EditMenu.exec($(this).attr('menu-new-path'), dish)
  }

  static updatePriceClick() {
    const dish = {
      id: $(this).attr('data-id'),
      price: $(this).parentsUntil('.menu-item-right').parent().children('.menu-item-price').children('.changePrice')
        .val()
    }
    EditMenu.exec($(this).attr('menu-update-path'), dish)
  }

  static deleteDishClick() {
    const dish = {
      id: $(this).attr('data-id')
    }
    EditMenu.exec($(this).attr('menu-delete-path'), dish)
  }

  static newCategoryClick() {
    const category = {
      name: $(this).parent().children('.newCat').val()
    }
    EditMenu.exec($(this).attr('menu-new-category-path'), category)
  }
  static deleteCategoryClick() {
    const category = {
      id: $(this).attr('data-id')
    }
    EditMenu.exec($(this).attr('menu-delete-category-path'), category)
  }
}
export default EditMenu
