import $ from 'jquery'

class Orderjs {
  constructor(path) {
    this._path = path
  }
  show(element) {
    const icon1 = 'fa-pause' /* received */
    const icon2 = 'fa-clock-o' /* processing */
    const icon3 = 'fa-check' /* delivered */
    const icon4 = 'fa-paper-plane-o' /* sent */
    let x
    let stateIcon
    $.post(this._path, (data) => {
      /* console.log(data) */
      for (x in data) {
        if (data[x] !== 0) {
          switch (data[x].state) {
            case 'received':
              stateIcon = icon1
              break
            case 'processing':
              stateIcon = icon2
              break
            case 'delivered':
              stateIcon = icon3
              break
            default:
              stateIcon = icon4
              break
          }
          $(element).append(`
            <div class= "box-content-v">
              <div class="order">
                <span class="col">Ordine N°: ${data[x].id}</span>
                <span class="fa ${stateIcon}" aria-hidden="true"></span>
              </div>
            </div>`
          )
        }
      }
    })
  }
  static showMore() {
    $(this).parent().next('.more_info').slideToggle('slow')
    if ($(this).children('.fa').hasClass('fa-plus-square-o')) {
      $(this).children('.fa').removeClass('fa-plus-square-o')
      $(this).children('.fa').addClass('fa-minus-square-o')
    } else {
      $(this).children('.fa').removeClass('fa-minus-square-o')
      $(this).children('.fa').addClass('fa-plus-square-o')
    }
  }
  static nextStep() {
    const element = $(this)
    const id = $(this).attr('id-var')
    const updatePath = $('.v').attr('data-update-root')
    const updateData = {
      id
    }
    $.post(updatePath, updateData, () => {
      $(element).parentsUntil('.box-content-v').parent().slideToggle('slow')
      if ($(element).parentsUntil('.box-content-v').parent().parent().is('#ordinia')) {
        $('#ordinib').append(`<div class="box-content-v" >${$(element).parentsUntil('.box-content-v').parent().html()} </div>`)
      }
      $(element).parentsUntil('.box-content-v').parent().detach()
    })
  }
}
export default Orderjs
