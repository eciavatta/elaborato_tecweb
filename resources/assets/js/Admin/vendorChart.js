import $ from 'jquery'
import Chart from 'chart.js'

((Chart) => {
  if (typeof Chart === 'undefined') {
    throw new Error('chart.js required')
  }
})(Chart)
const CATEGORY = '2'
let chart = null
const opt = {
  type: 'line',
  data: {
    labels: [],
    datasets: [{
      label: '',
      data: [],
      backgroundColor: [
        'rgba(246, 52, 66, 0.2)'
      ],
      borderColor: [
        'rgba(246,52,66,1)'
      ],
      borderWidth: 1
    }]
  },
  options: {
    elements: {
      line: {
        tension: 0
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    }
  }
}
const temps = Object.assign({}, opt)
class VendorChart {

  constructor(element, path) {
    this._element = element
    this._path = path
  }
  update(value) {
    const ctx = this._element.getContext('2d')
    $.post(this._path, value, (data) => {
      const labels2 = []
      const value2 = []
      let x
      /* Lettura variabili dal file Json e popolamento vettori */
      const temp = $.extend(true, {}, temps)
      if (value.ascisse === CATEGORY) {
        temp.type = 'bar'
      } else {
        temp.type = 'line'
      }
      for (x in data) {
        if (data[x] !== 0) {
          labels2.push(x)
          value2.push(data[x])
        }
      }
      temp.data.labels = labels2
      temp.data.datasets.forEach((dataset) => {
        while (dataset.data.length > 0) {
          dataset.data.pop()
        }
      })
      value2.forEach((value) => {
        temp.data.datasets['0'].data.push(value)
      })
      chart.destroy()
      chart = new Chart(ctx, temp)
      return chart
    })
  }
  show(value) {
    const ctx = this._element.getContext('2d')
    $.post(this._path, value, (data) => {
      const labels = []
      const value = []
      let x
      /* Lettura variabili dal file Json e popolamento vettori */
      for (x in data) {
        if (data[x] !== 0) {
          labels.push(x)
          value.push(data[x])
        }
      }
      opt.data.labels = labels
      opt.data.datasets.forEach((dataset) => {
        value.forEach((value1) => {
          dataset.data.push(value1)
        })
      })
      /* dati e opzioni vari del grafico */
      chart = new Chart(ctx, opt)
      return chart
    })
  }
}

export default VendorChart
