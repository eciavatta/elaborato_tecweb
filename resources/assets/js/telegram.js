import $ from 'jquery'

class Telegram {

  init() {
    if (!$('#telegram_sync').length) {
      return
    }

    this.interval = 500
    this._button = $('#telegram_sync')
    this._syncUrl = this._button.data('sync-route')
    this._button.click(() => {
      this._button.find('.fa').removeClass('d-none')
      this._button.find('.text').text('Sincronizzazione in corso..')
      this._polling = setInterval(() => this._checkSyncDone(), this.interval)
    })
  }

  _checkSyncDone() {
    $.ajax({
      url: this._syncUrl,
      type: 'GET',
      success: (data) => {
        if (!data.result) {
          return
        }

        clearInterval(this._polling)
        this._button.find('.fa').removeClass('fa-spinner fa-pulse fa-3x fa-fw')
        this._button.find('.fa').addClass('fa-check')
        this._button.find('.text').text('Sincronizzazione completata')
        this._button.addClass('disabled')
        this._button.attr('aria-disabled', true)
      }
    })
  }
}

export default Telegram
