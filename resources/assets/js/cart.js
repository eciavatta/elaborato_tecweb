import $ from 'jquery'

class Cart {

  init() {
    if (!$('.cart-dishes').length) {
      return
    }

    const executeAction = this._executeAction
    const reloadAfter = 500

    setTimeout(() => {
      $('.total-price-value').text($('.total-price').text())
      $('.header-total-price .spinner-icon').hide()
    }, reloadAfter)

    $('.menu-item-add').click(function () {
      executeAction('PUT', $(this).data('id'), executeAction)
    })

    $('.cart-item-remove').click(function () {
      executeAction('DELETE', $(this).data('id'), executeAction)
    })
  }

  _executeAction(type, dishId, callback) {
    const reloadAfter = 500
    let url = ''
    if (type === 'PUT') {
      url = $('.menu-items').data('add-route')
    } else if (type === 'DELETE') {
      url = $('.cart-dishes').data('remove-route')
    }

    $.ajax({
      url,
      type,
      data: `dishId=${dishId}`,
      success: (data) => {
        $('.cart-summary').html(data)
        $('.cart-item-remove').click(function () {
          callback('DELETE', $(this).data('id'), callback)
        })

        $('.total-price-value').hide()
        $('.header-total-price .spinner-icon').show()

        setTimeout(() => {
          $('.total-price-value').text($('.total-price').text())
          $('.header-total-price .spinner-icon').hide()
          $('.total-price-value').show()
        }, reloadAfter)
      }
    })
  }
}

export default Cart
