import $ from 'jquery'
import CartController from './cart'
import EditMenu from './Admin/editMenu'
import Orderjs from './Admin/order'
import TelegramController from './telegram'
import Util from './util'
import VendorChart from './Admin/vendorChart'

(($) => {
  if (typeof $ === 'undefined') {
    throw new Error('jQuery requiredd')
  }
})($)

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})

$(document).ready(() => {
  const cartController = new CartController()
  cartController.init()

  const telegramController = new TelegramController()
  telegramController.init()

  if ($('#myChart1').length) {
    const n = 30
    const chart1 = new VendorChart($('#myChart1')[0], $('#myChart1').attr('data-analytics-route'))
    chart1.show({
      ascisse: 1,
      ordinate: 1,
      numberOfDay: n
    })
  }
  /* Fine caricamento dinamico grafici */

  if ($('.i-b-a').length) {
    const order = new Orderjs($('.i-b-a').attr('data-order-route'))
    order.show($('.i-b-a')[0])
  }

  if ($('.order-page').length) {
    $('.more').click(Orderjs.showMore)
    $('.update').click(Orderjs.nextStep)
  }

  if ($('.range').length) {
    $('.mb').text($('.range').val())
    $('.range').change(() => {
      $('.mb').text($('.range').val())
    })
  }

  if ($('.mbtn').length) {
    $('.mbtn').click(() => {
      const value = {
        ascisse: $('#x').val(),
        ordinate: $('#y').val(),
        numberOfDay: $('.range').val()
      }
      const chart = new VendorChart($('#myChart1')[0], $('#myChart1').attr('data-analytics-route'))
      chart.update(value)
    })
  }
  if ($('.editForm').length) {
    $('.newDish').click(EditMenu.newDishClick)
  }
  if ($('.changePriceBtn').length) {
    $('.changePriceBtn').click(EditMenu.updatePriceClick)
  }
  if ($('.deleteDish').length) {
    $('.deleteDish').click(EditMenu.deleteDishClick)
  }
  if ($('.newCategories').length) {
    $('.newCategories').click(EditMenu.newCategoryClick)
  }
  if ($('.delCategories').length) {
    $('.delCategories').click(EditMenu.deleteCategoryClick)
  }

})

export {
  CartController,
  EditMenu,
  Orderjs,
  TelegramController,
  Util,
  VendorChart
}
