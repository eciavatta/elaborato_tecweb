<div class="cart-dishes" data-remove-route="{{ route('cart.remove') }}" data-total-price="{{ $totalPrice }}">
    @if (session('cart'))
        @foreach (session('cart') as $id => $dish)
            <div class="cart-dish">
                @if (!$locked)
                    <div class="cart-item-actions" data-id="{{ $id }}" data-quantity="{{ $dish['quantity'] }}">
                        <button class="cart-item-remove btn btn-custom btn-custom-secondary-2" data-id="{{ $id }}">
                            <span class="fa fa-minus" aria-hidden="true"></span>
                        </button>
                    </div>
                @endif

                <div class="cart-item-quantity">
                    {{ $dish['quantity'] }}
                </div>

                <div class="cart-item-separator">x</div>

                <div class="cart-item-name">
                    {{ $dish['name'] }}
                </div>

                <div class="cart-item-price">
                    &euro; {{ number_format($dish['price'] * $dish['quantity'] / 100, 2) }}
                </div>

                <div style="clear: both;"></div>
            </div>
        @endforeach
    @endif

    <div class="cart-totals">
        <div class="cart-subtotal">
            <span>Subtotale</span>
            <span style="float: right">&euro; {{ number_format($totalPrice / 100, 2) }}</span>
        </div>

        <div class="cart-shipping">
            <span>Costo di consegna</span>
            <span style="float: right">&euro; 0.00</span>
        </div>

        <div class="cart-total">
            <span>Totale</span>
            <span class="total-price" style="float: right">&euro; {{ number_format($totalPrice / 100, 2) }}</span>
        </div>
    </div>

    @if ($locked)
        <div class="cart-cancel">
            <a href="{{ route('menu') }}" class="btn btn-custom btn-custom-secondary-2 btn-full-mobile" style="color: #fff">Modifica ordine</a>
        </div>
    @elseif ($totalPrice > 0)
        <div class="cart-order">
            <form method="POST" action="{{ route('cart.buy') }}">
                {{ csrf_field() }}

                <div class="cart-order-delivery">
                <button type="submit" name="pickup" class="btn btn-custom btn-custom-secondary-1 btn-full">Ordina e ritira</button>
                </div>

                <div class="cart-order-separator">
                    oppure
                </div>

                <div class="cart-order-pickup">
                    <button type="submit" name="delivery" class="btn btn-custom btn-custom-primary btn-full{{ $totalPrice < 1500 ? ' disabled' : '' }}"{{ $totalPrice < 1500 ? ' disabled' : '' }}>Ordina e ricevi</button>
                    <span class="d-block">Consegna sopra &euro; 15.00</span>
                </div>
            </form>
        </div>

    @endif
</div>
