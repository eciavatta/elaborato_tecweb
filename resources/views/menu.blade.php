@extends('layouts.app')
@section('title', 'Menù - '.config('app.name', 'Cesena Food'))

@section('content')
<main class="main-container with-header">
    <div class="menu-wrapper">
        <header class="page-title">
            <h2 class="title">Menù</h2>
            <div class="description">Scegli con cura che cosa ordinare</div>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-3 d-none d-lg-block">
                    <div class="menu-block">
                        <div class="menu-title">
                            <h3 class="title">
                                <span class="fa fa-bars" aria-hidden="true"></span>Categorie
                            </h3>
                        </div>

                        <div class="category-list">
                            <ul>
                                @foreach ($categories as $category)
                                @if($category->active)
                                    <li><a href="#category{{ $loop->index }}">{{ $category->name }}</a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="menu-block">
                        <div class="menu-title">
                            <h3 class="title">
                            <span class="fa fa-cutlery" aria-hidden="true"></span>Listino
                            </h3>
                        </div>

                        <div class="menu-list">
                            @foreach ($categories as $category)
                            @if($category->active)
                                <div class="menu-category" id="category{{ $loop->index }}">
                                    <h3 class="title">{{ $category->name }}</h3>
                                </div>

                                <div class="menu-items" data-add-route="{{ route('cart.add') }}">
                                    @foreach ($category->dishes as $dish)
                                    @if($dish->available)
                                        <div class="menu-item">
                                            <div class="menu-item-left">
                                                <div class="menu-item-title">
                                                    {{ $dish->name }}
                                                </div>
                                                <div class="menu-item-description d-none d-lg-block">
                                                    {{ $dish->description }}
                                                </div>
                                            </div>

                                            <div class="menu-item-right">
                                                <div class="menu-item-price">
                                                    &euro; {{ number_format($dish->price / 100, 2) }}
                                                </div>

                                                <div class="menu-item-actions">
                                                    <button class="menu-item-add btn btn-custom btn-custom-primary" data-id="{{ $dish->id }}">
                                                        <span class="fa fa-cart-plus" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </div>

                                            <div style="clear: both;"></div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-6 col-lg-3 d-none d-md-block">
                    <div class="menu-block">
                        <div class="menu-title">
                            <h3 class="title">
                                <span class="fa fa-shopping-cart" aria-hidden="true"></span>
                                Riepilogo ordine
                            </h3>
                        </div>

                        <div class="cart-summary">
                            @component('cart/summary', ['totalPrice' => $totalPrice, 'locked' => false])
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
