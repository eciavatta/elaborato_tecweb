<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!--
      CesenaFood v{{ config('app.version') }}

      Written by Emiliano Ciavatta and Simone Golinucci
      This work is licensed under a Creative Commons Attribution 4.0 International License
      <https://creativecommons.org/licenses/by/4.0/>
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name="description" content="Elaborato di progetto per l'esame del corso di Tecnologie Web">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#fcc235">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#fcc235">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#fcc235">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,700,600,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@env('production')
    <link rel="stylesheet" href="{{ asset('css/style.min.css') }}@version">
@else
    <link rel="stylesheet" href="{{ asset('css/style.css') }}@version">
@endenv
</head>

<body>
@section('header')
    <header class="main-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <section class="logo">
                        <h1><a href="{{ route('home') }}">{{ config('app.name') }}</a></h1>
                    </section>
                </div>
                <div class="col-auto">
                    <div class="header-actions">
                        @route('menu|cart|order*')
                            <a href="{{ route('cart') }}" onclick="return $(window).width() < 576;">
                                <div class="action-container">
                                    <span class="action-label">
                                        <span class="header-total-price">
                                            <span class="fa fa-spinner fa-pulse fa-3x fa-fw spinner-icon"></span>
                                            <span class="total-price-value"></span>
                                        </span>
                                        <span class="fa fa-shopping-cart" aria-hidden="true"></span>
                                    </span>
                                </div>
                            </a>
                        @else
                            @guest
                                <a href="{{ route('login') }}">
                                    <div class="action-container">
                                        <span class="action-label">Accedi</span>
                                    </div>
                                </a>
                            @else
                                <a href="{{ route('profile') }}">
                                    <div class="action-container">
                                        <span class="action-label">Profilo</span>
                                    </div>
                                </a>
                            @endguest
                        @endroute
                    </div>
                </div>
            </div>
        </div>
    </header>
@show

@yield('content')

@section('footer')
    <footer class="main-footer">
        <div class="container">
            <section class="links">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="footer-group-title">
                            <h4>Iscriviti per offerte speciali</h4>
                        </div>
                        <div class="footer-group-content">
                            <form action="{{ route('newsletter/subscribe') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="newsletterEmail" class="sr-only">Email address</label>
                                    <input type="email" class="form-control footer-text-input" id="newsletterEmail" placeholder="Indirizzo email" required>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input footer-check-input" id="newsletterAgreement" required>
                                    <label class="form-check-label" for="newsletterAgreement">Acconsento al trattamento dei dati secondo l'<a href="{{ route('privacy-policy') }}">informativa</a></label>
                                </div>
                                <button type="submit" class="btn footer-btn">Inviami aggiornamenti</button>
                            </form>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="footer-group-title">
                            <h4>Conoscici meglio</h4>
                        </div>
                        <div class="footer-group-content">
                            <ul class="footer-links">
                                <li><a href="#">Chi siamo</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#">Lavora con noi</a></li>
                                <li><a href="#">Le nostre lavorazioni</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="footer-group-title">
                            <h4>Link utili</h4>
                        </div>
                        <div class="footer-group-content">
                            <ul class="footer-links">
                                <li><a href="#">Portale Unibo</a></li>
                                <li><a href="#">Convenzioni studenti</a></li>
                                <li><a href="#">Domande frequenti</a></li>
                                <li><a href="#">Contattaci</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="footer-group-title">
                            <h4>Resta connesso con noi!</h4>
                        </div>
                        <div class="footer-group-content">
                            <ul class="footer-icons">
                                <li><a href="#"><span class="fa fa-facebook-official" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="fa fa-google-plus-official" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="fa fa-tripadvisor" aria-hidden="true"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <hr>

            <div class="copyrights">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="text-center text-md-left">&copy; 2018 {{ config('app.name') }} Tutti i diritti riservati.</div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="text-center text-md-right">
                            <a style="margin-right: 2px" href="{{ route('terms-of-use') }}">Termini d'uso</a>
                            <a style="margin-left: 2px" href="{{ route('privacy-policy') }}">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
@debug
            <div class="debug">Versione: <strong>{{ config('app.version') }}</strong>. Ambiente: <strong>{{ config('app.env') }}</strong>. Debug attivato: <strong>{{ config('app.debug') ? 'true' : 'false' }}</strong>. Tempo di rendering: <strong>{{ defined('LARAVEL_START') ? round(microtime(true) - LARAVEL_START, 3) : '' }} secondi</strong>.</div>
@enddebug
        </div>
    </footer>
@show
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" crossorigin="anonymous"></script>
@env('production')
    <script src="{{ asset('js/script.min.js') }}@version"></script>
@else
    <script src="{{ asset('js/script.js') }}@version"></script>
@endenv
</body>

</html>
