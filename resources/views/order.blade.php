@extends('layouts.app')
@section('title', 'Effettua ordine - '.config('app.name', 'Cesena Food'))

@section('header')

@section('content')
<main class="main-container with-header">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-auto col-md-9">
                <header class="join-header">
                    @if ($step1)
                        <h2>Ci servono altre informazioni</h2>
                        <h3>Indicaci orario e dove vuoi ritirare il tuo pasto</h3>
                    @elseif ($step2)
                        <h2>Ci siamo quasi</h2>
                        <h3>Inserisci informazioni aggiuntive</h3>
                    @elseif ($step3)
                        <h2>Pagamento effettuato</h2>
                        <h3>Stiamo già preparando il tuo pasto!</h3>
                    @endif
                </header>

                <div class="join-steps">
                    <div class="row">
                        @if ($step1)
                            <div class="col-sm col-md-4 padding-right-0">
                        @else
                            <div class="d-none d-md-block col-md-4 padding-right-0">
                        @endif
                            <div class="step with-border{{ $step1 ? ' current' : ' success' }}">
                                <div class="step-icon">
                                    <span class="fa fa-{{ $step1 ? 'calendar' : 'check' }}" aria-hidden="true"></span>
                                </div>
                                <div class="step-text">
                                    <div class="step-title">
                                        @if ($step1)
                                            Passo 1<span class="d-md-none">/3</span>:
                                        @else
                                            Completato
                                        @endif
                                    </div>
                                    <div class="step-description">Orario e luogo</div>
                                </div>
                            </div>
                        </div>

                        @if ($step2)
                            <div class="col-sm col-md-4 padding-0">
                        @else
                            <div class="d-none d-md-block col-md-4 padding-0">
                        @endif
                            <div class="step with-border with-padding{{ $step1 ? ' disabled' : ($step2 ? ' current' : ' success') }}">
                                <div class="step-icon">
                                    <span class="fa fa-{{ ($step1 or $step2) ? 'credit-card' : 'check' }}" aria-hidden="true"></span>
                                </div>
                                <div class="step-text">
                                    <div class="step-title">
                                        @if ($step1 or $step2)
                                            Passo 2<span class="d-md-none">/3</span>:
                                        @else
                                            Completato
                                        @endif
                                    </div>
                                    <div class="step-description">Metodo di pagamento</div>
                                </div>
                            </div>
                        </div>

                        @if ($step3)
                            <div class="col-sm col-md-4 padding-0">
                        @else
                            <div class="d-none d-md-block col-md-4 padding-0">
                        @endif
                            <div class="step with-padding{{ $step3 ? ' current' : ' disabled' }}">
                                <div class="step-icon">
                                    <span class="fa fa-cutlery" aria-hidden="true"></span>
                                </div>
                                <div class="step-text">
                                    <div class="step-title">Passo 3<span class="d-md-none">/3</span>:</div>
                                    <div class="step-description">Ordine completato</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="join-form-container">
                    <div class="row">
                        <div class="col-sm col-md-6">
                            <header class="join-form-header">
                                @if ($step1 and session('delivery'))
                                    <h3>Hai scelto la consegna a casa</h3>
                                @elseif ($step1 and !session('delivery'))
                                    <h3>Hai scelto il ritiro a mano</h3>
                                @elseif ($step2)
                                    <h3>Scegli il metodo di pagamento che preferisci</h3>
                                @elseif ($step3)
                                    <h3>Ordine effettuato con successo!</h3>
                                @endif
                            </header>

                            @if (!$errors->isEmpty())
                                <div class="join-form-errors">
                                    @component('components/alert', ['type' => 'danger'])
                                        @if ($errors->has('address'))
                                            {{ $errors->first('address') }}
                                        @elseif ($errors->has('time'))
                                            {{ $errors->first('time') }}
                                        @elseif ($errors->has('payment'))
                                            {{ $errors->first('payment') }}
                                        @endif
                                    @endcomponent
                                </div>
                            @endif

                            <div class="join-form">
                                <form method="POST" action="{{ $step1 ? route('order') : ($step2 ? route('order.payments') : route('order.complete')) }}">
                                    {{ csrf_field() }}

                                    @if ($step1)
                                        @if (session('delivery'))
                                            <div class="form-group">
                                                <label for="address" class="control-label control-label-custom">Indirizzo di consegna</label>
                                                <input id="address" type="text" class="form-control input-field-custom" name="address" value="{{ old('address') }}" required autofocus>
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <label for="time" class="control-label control-label-custom">Orario di consegna</label>
                                            <input id="time" type="time" class="form-control input-field-custom" name="time" value="{{ old('time') }}" required{{ !session('delivery') ? ' autofocus' : '' }}>
                                        </div>

                                        <div class="form-group">
                                            <label for="note" class="control-label control-label-custom">Ulteriori note</label>
                                            <textarea id="note" class="form-control input-field-custom" name="note" value="{{ old('note') }}"></textarea>
                                        </div>
                                    @elseif ($step2)
                                        <div class="form-group check-group">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="payment" id="cash" value="cash" checked>
                                                <label class="form-check-label" for="cash">
                                                    <span class="fa fa-money check-icon" aria-hidden="true"></span>Contanti al ritiro o alla consegna
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="payment" id="creditCard" value="creditCard">
                                                <label class="form-check-label" for="creditCard">
                                                    <span class="fa fa-credit-card check-icon" aria-hidden="true"></span>Carta di credito
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="payment" id="paypal" value="paypal">
                                                <label class="form-check-label" for="paypal">
                                                    <span class="fa fa-paypal check-icon" aria-hidden="true"></span>Pagamento con Paypal
                                                </label>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-primary btn-full-mobile">{{ $step1 ? 'Continua' : ($step2 ? 'Paga ora' : 'Continua su '.config('app.name', 'Cesena Food')) }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="d-none d-md-block col-md-6">
                            @if ($step1 or $step2)
                                <div class="menu-block">
                                    <div class="menu-title">
                                        <h3 class="title">
                                            <span class="fa fa-shopping-cart" aria-hidden="true"></span>
                                            Riepilogo ordine
                                        </h3>
                                    </div>

                                    <div class="cart-summary">
                                        @component('cart/summary', ['totalPrice' => $totalPrice, 'locked' => true])
                                        @endcomponent
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection
