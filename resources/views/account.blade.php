@extends('layouts.app')
@section('title', 'Modifica profilo - '.config('app.name', 'Cesena Food'))

@section('header')

@section('content')
<main class="main-container with-header profile-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-auto col-md-9">
                <header class="profile-header">
                    <h2>Modifica profilo</h2>
                </header>


                <div class="profile-form-alerts">
                    @if (session('success'))
                        @component('components/alert', ['type' => 'success'])
                            {{ session('success') }}
                        @endcomponent
                    @endif

                    @if (!$errors->isEmpty())
                        @component('components/alert', ['type' => 'danger'])
                            @if ($errors->has('firstname'))
                                {{ $errors->first('firstname') }}
                            @elseif ($errors->has('lastname'))
                                {{ $errors->first('lastname') }}
                            @elseif ($errors->has('email'))
                                {{ $errors->first('email') }}
                            @elseif ($errors->has('password'))
                                {{ $errors->first('password') }}
                            @elseif ($errors->has('address'))
                                {{ $errors->first('address') }}
                            @elseif ($errors->has('telephone'))
                                {{ $errors->first('telephone') }}
                            @endif
                        @endcomponent
                    @endif
                </div>

                <div class="row">
                    <div class="col-sm col-md-6">
                        <div class="profile-form-container">
                            <header class="profile-form-header">
                                <h3>Informazioni generali</h3>
                            </header>

                            <div class="profile-form">
                                <form method="POST" action="{{ route('profile') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="update" value="general_information">

                                    <div class="form-group">
                                        <label for="firstname" class="control-label control-label-custom control-label control-label-custom-custom">Nome</label>
                                        <input id="firstname" type="text" class="form-control input-field-custom" name="firstname" value="{{ old('firstname') ?? $user->firstname }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="control-label control-label-custom control-label control-label-custom-custom">Cognome</label>
                                        <input id="lastname" type="text" class="form-control input-field-custom" name="lastname" value="{{ old('lastname') ?? $user->lastname }}" required>
                                    </div>

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-primary btn-full-mobile">Salva modifiche</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="profile-form-container">
                            <header class="profile-form-header">
                                <h3>Informazioni aggiuntive</h3>
                            </header>

                            <div class="profile-form">
                                <form method="POST" action="{{ route('profile') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="update" value="additional_information">

                                    <div class="form-group">
                                        <label for="address" class="control-label control-label-custom control-label control-label-custom-custom">Indirizzo</label>
                                        <input id="address" type="text" class="form-control input-field-custom" name="address" value="{{ old('address') ?? $user->address }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="telephone" class="control-label control-label-custom control-label control-label-custom-custom">Telefono</label>
                                        <input id="telephone" type="text" class="form-control input-field-custom" name="telephone" value="{{ old('telephone') ?? $user->telephone }}">
                                    </div>

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-primary btn-full-mobile">Salva modifiche</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="profile-form-container">
                            <header class="profile-form-header">
                                <h3>Disconnetti</h3>
                            </header>

                            <div class="profile-form">
                                <form method="POST" action="{{ route('logout') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-secondary-1 btn-full-mobile">Disconnettiti da questo dispositivo</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm col-md-6">
                        <div class="profile-form-container">
                            <header class="profile-form-header">
                                <h3>Cambia indirizzo email</h3>
                            </header>

                            <div class="profile-form">
                                <form method="POST" action="{{ route('profile') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="update" value="change_email">

                                    <div class="form-group">
                                        <label for="email" class="control-label control-label-custom control-label control-label-custom-custom">Nuovo indirizzo email</label>
                                        <input id="email" type="email" class="form-control input-field-custom" name="email" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email-confirm" class="control-label control-label-custom control-label control-label-custom-custom">Ripeti indirizzo email</label>
                                        <input id="email-confirm" type="email" class="form-control input-field-custom" name="email_confirmation" required>
                                    </div>

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-primary btn-full-mobile">Modifica email</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="profile-form-container">
                            <header class="profile-form-header">
                                <h3>Cambia password</h3>
                            </header>

                            <div class="profile-form">
                                <form method="POST" action="{{ route('profile') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="update" value="change_password">

                                    <div class="form-group">
                                        <label for="password" class="control-label control-label-custom control-label control-label-custom-custom">Password</label>
                                        <input id="password" type="password" class="form-control input-field-custom" name="password" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm" class="control-label control-label-custom control-label control-label-custom-custom">Conferma Password</label>
                                        <input id="password-confirm" type="password" class="form-control input-field-custom" name="password_confirmation" required>
                                    </div>

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-primary btn-full-mobile">Modifica password</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="profile-form-container">
                            <header class="profile-form-header">
                                <h3>Elimina account</h3>
                            </header>

                            <div class="profile-form control-group">
                                <a href="{{ route('account.delete') }}" class="btn btn-custom btn-custom-secondary-2 btn-full-mobile">Elimina account definitivamente</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection
