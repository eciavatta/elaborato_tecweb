<div class="row">
<div class="col-12">
    <h3>Ordini - @php echo date('d/m/Y'); @endphp</h3>
</div>
<div class="col-12 col-md-6">
    <div class="info-box" id="ordinia">
        <div class="box-image"></div>
        <div class="box-opacity-v"></div>
        <div class="box-title">
            <span class="fa fa-pause" aria-hidden="true"></span>
            <h5>Ordini ricevuti</h5>
        </div>
        @if (session('orders'))
            @foreach (session('orders') as $order)
                @if ($order->state == "received")
                    <div class="box-content-v" >
                        <div class="order">
                            <span class="col"><strong>{{$order->id}}</strong></span>
                            <span class="col">{{$order->update_time}}</span>
                            <a class="more"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                        </div>
                        <div class="more_info">
                            <table class="table">
                                <tr>
                                    <th class="col">Piatto</th>
                                    <th class="col">Quantità</th>
                                    <th class="col">Prezzo</th>
                                </tr>
                                @foreach ($order->dishes as $dish)
                                    <tr>
                                        <td>{{$dish->name}}</td>
                                        <td>{{$dish->pivot->quantity}}</td>
                                        <td>{{$dish->pivot->single_price}}</td>
                                    </tr>
                                @endforeach

                            </table>
                            <table class="table user">
                                <tr>
                                    <th class="col">Nome</th>
                                    <th class="col">Email</th>
                                </tr>
                                <tr>
                                    <td>{{$order->user->firstname}}</th>
                                    <td>{{$order->user->email}}</th>
                                </tr>
                            </table>
                            <button class="btn update" id-var="{{$order->id}}">next</button>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div><!--col-12-->
<div class="col-12 col-md-6">
    <div class="info-box" id="ordinib">
        <div class="box-image"></div>
        <div class="box-opacity-v"></div>
        <div class="box-title">
            <span class="fa fa-pause" aria-hidden="true"></span>
            <h5>Ordini in lavorazione</h5>
        </div>
            @if (session('orders'))
            @foreach (session('orders') as $order)
                @if ($order->state == "processing")
                    <div class="box-content-v" >
                        <div class="order">
                            <span class="col"><strong>{{$order->id}}</strong></span>
                            <span class="col">{{$order->update_time}}</span>
                            <a class="more"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                        </div>
                        <div class="more_info">
                            <table class="table">
                                <tr>
                                    <th class="col">Piatto</th>
                                    <th class="col">Quantità</th>
                                    <th class="col">Prezzo</th>
                                </tr>
                                @foreach ($order->dishes as $dish)
                                    <tr>
                                        <td>{{$dish->name}}</td>
                                        <td>{{$dish->pivot->quantity}}</td>
                                        <td>{{$dish->pivot->single_price}}</td>
                                    </tr>
                                @endforeach
                            </table>
                            <table class="table user">
                                <tr>
                                    <th class="col">Nome</th>
                                    <th class="col">Email</th>
                                </tr>
                                <tr>
                                    <td>{{$order->user->firstname}}</th>
                                    <td>{{$order->user->email}}</th>
                                </tr>
                            </table>
                            <button class="btn update" id-var="{{$order->id}}">next</button>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
        </div>
    </div>
</div><!--col-12-->
</div>
