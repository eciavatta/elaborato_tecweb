@extends('layouts.app')
@section('title', 'Utenti - '.config('app.name', 'Cesena Food'))

@section('header')

@section('content')
<main class="main-v">
    <section class="container v order-page" data-update-root="{{ route('updateOrder') }}">
        <div class="row">
            <div class="col-12 col-md-3 navigator">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('analyticPage')}}">Statistiche</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('orderPage')}}">Ordini</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('userPage')}}">Utenti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('editPage')}}">Menu</a>
                    </li>
                </ul>
            </div>
            <!--Sezione degli ordini-->
            <div class="col-12 col-md-9 lista">
                <div class="row">
                    @if (session('utenti'))
                        @foreach (session('utenti') as $user)
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{$user->firstname}} {{$user->lastname}}</h5>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><span>Email: </span>{{$user->email}}</li>
                                        <li class="list-group-item"><span>Indirizzo: </span>{{$user->address}}</li>
                                        <li class="list-group-item"><span>Data di registrazione: </span>{{$user->created_at}}</li>
                                    </ul>
                                    <a href="#" class="btn mbtn">Fai Qualcosa</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div><!--row-->
    </section>
</main>
@endsection

@section('footer')
@endsection