@extends('layouts.app')
@section('title', 'Ordini - '.config('app.name', 'Cesena Food'))

@section('header')

@section('content')
<main class="main-v">
    <section class="container v order-page" data-update-root="{{ route('updateOrder') }}">
        <div class="row">
            <div class="col-12 col-md-3 navigator">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('analyticPage')}}">Statistiche</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('orderPage')}}">Ordini</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('userPage')}}">Utenti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('editPage')}}">Menu</a>
                    </li>
                </ul>
            </div>
            <!--Sezione degli ordini-->
            <div class="col-12 col-md-9 lista">
                @component('admin/order/orderLoader')
                @endcomponent
            </div>
        </div><!--row-->
    </section>
</main>
@endsection

@section('footer')
@endsection
