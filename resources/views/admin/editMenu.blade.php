@extends('layouts.app')
@section('title', 'Utenti - '.config('app.name', 'Cesena Food'))

@section('header')

@section('content')
<main class="main-v">
    <section class="container v order-page" data-update-root="{{ route('updateOrder') }}">
        <div class="row">
            <div class="col-12 col-md-3 navigator">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('analyticPage')}}">Statistiche</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('orderPage')}}">Ordini</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('userPage')}}">Utenti</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('editPage')}}">Menu</a>
                    </li>
                    <li class="nav-item">
                        <h6>Categorie</h6>
                        <div class="category-list edit-category-list">
                            <ul>
                                @foreach ($categories as $category)
                                    @if ($category->active)
                                        <li>
                                            <a href="#category{{ $loop->index }}">{{ $category->name }}</a>
                                            <button class="editMenu-item-add btn btn-custom btn-custom-primary delCategories" id="delCategories" menu-delete-category-path="{{route('deleteCategory')}}" data-id="{{$category->id}}">
                                                <span class="fa fa-trash" aria-hidden="true"></span>
                                            </button>
                                        </li>
                                    @endif
                                @endforeach
                                <li>
                                    <div class="newCategory-group">
                                        <label for="newCat" class="sr-only">NomeCategoria</label>
                                        <input type="text" id="newCat" class="form-control m-i-t newCat" placeholder="Aggiungi"/>
                                        <button class="editMenu-item-add btn btn-custom btn-custom-primary newCategories" id="newCategories" menu-new-category-path="{{route('newCategory')}}">
                                                <span class="fa fa-plus" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-9 lista">
                <div class="edit-menu-block">
                    <div class="menu-title">
                        <h3 class="title">
                        <span class="fa fa-cutlery" aria-hidden="true"></span>Listino
                        </h3>
                    </div>
                    <div class="alert message" role="alert">
                    </div>
                    <div class="menu-list">
                        @foreach ($categories as $category)
                            @if ($category->active)
                            <div class="menu-category" id="category{{ $loop->index }}">
                                <h3 class="title">{{ $category->name }}</h3>
                            </div>

                            <div class="menu-items" data-add-route="{{ route('cart.add') }}">
                                @foreach ($category->dishes as $dish)
                                    @if($dish->available)
                                    <div class="menu-item">
                                        <div class="menu-item-left">
                                            <div class="menu-item-title">
                                                {{ $dish->name }}
                                            </div>
                                            <div class="menu-item-description d-none d-lg-block">
                                                {{ $dish->description }}
                                            </div>
                                        </div>

                                        <div class="menu-item-right">
                                            <div class="menu-item-price">
                                                <label for="changePrice" class="sr-only">Euro</label>
                                                <input type="number" id="changePrice" class="form-control m-i-t changePrice" value="{{ $dish->price / 100 }}"/>
                                            </div>

                                            <div class="menu-item-actions">
                                                <button class="editMenu-item-add btn btn-custom btn-custom-primary change-price-icon changePriceBtn" id="changePriceBtn" menu-update-path="{{route('updatePrice')}}" data-id="{{ $dish->id }}">
                                                    <span class="fa fa-pencil-square-o" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <div class="menu-item-actions">
                                                <button class="editMenu-item-add btn btn-custom btn-custom-primary deleteDish" id="deleteDish" menu-delete-path="{{route('deleteDish')}}" data-id="{{ $dish->id }}">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                        </div>

                                        <div style="clear: both;"></div>
                                    </div>
                                    @endif
                                @endforeach
                                <div category="{{$category->id}} " class="editForm">
                                    <div class="form-group">
                                        <h6>Aggiungi piatto in questa categoria</h6>
                                        <label for="dish" class="sr-only">nome del piatto</label><input id="dish" type="text" class="form-control m-i-t dish" placeholder="Nome Piatto"/>
                                        <label for="price" class="sr-only">prezzo</label><input id="price" type="number" class="form-control m-i-t price" placeholder="Prezzo"/>
                                        <label for="description" class="sr-only">Ingredienti del piatto</label>
                                        <textarea class="form-control description" id="description" placeholder="Ingredienti del piatto separatti da una virgola (,)"></textarea>
                                        <div class="menu-item-actions">
                                            <label for="newDish" class="sr-only">Premi per aggiungere nuovo piatto</label>
                                            <button class="editMenu-item-add btn btn-custom btn-custom-add newDish" id="newDish" data-id="{{ $dish->id }}" menu-new-path="{{route('newDish')}}">
                                                <span class="fa fa-plus" aria-hidden="true"></span>
                                                Aggiungi al menú
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@section('footer')
@endsection
