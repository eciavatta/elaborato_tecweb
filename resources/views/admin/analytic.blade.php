@extends('layouts.app')
@section('title', 'Analytic - '.config('app.name', 'Cesena Food'))

@section('header')
@section('content')
<main class="main-v">
    <section class="container v">
        <div class="row">
            <div class="col-12 col-md-3 navigator">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin')}}">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('analyticPage')}}">Statistiche</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('orderPage')}}">Ordini</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('userPage')}}">Utenti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('editPage')}}">Menu</a>
                    </li>
                    <li class="nav-item">
                        <h6>Seleziona grafico</h6>
                        <form class="analytic-form">
                            <select name="x" id="x">
                                <option value="1" default>Data</option>
                                <option value="2">Categoria</option>
                            </select>
                            <select name="y" id="y">
                                <option value="1" default>N°Ordini</option>
                                <option value="2">Incasso</option>
                            </select>
                            <div class="range-div">
                                <span>
                                    N° Giorni
                                    <span class="badge mb">00</span>
                                </span>
                                <input type="range" class="range" value="30" min="1" max="30">
                            </div>
                            <button type='button' class="btn mbtn">Invia</button>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="col-12">
                        <h3>Statistiche Generali</h3>
                    </div>
                    <!--Sezione delle statistiche-->
                    <div class="col-12">
                        <div class="info-box">
                            <div class="box-image"></div>
                            <div class="box-opacity-v"></div>
                            <div class="box-title">
                                <span class="fa fa-line-chart" aria-hidden="true"></span>
                                <h5>Statistiche - Ordini per giorno</h5>
                            </div>
                            <div class="box-content-v">
                                <canvas id="myChart1" data-analytics-route="{{ route('getNAnalytic') }}"></canvas>
                            </div>
                        </div><!--info-box-->
                    </div><!--col-12-->
                </div><!--row2-->
            </div>
        </div><!--row-->
    </section>
</main>
@endsection

@section('footer')
@endsection
