@extends('layouts.app')
@section('title', 'Admin - '.config('app.name', 'Cesena Food'))

@section('header')
@section('content')
<main class="main-v">
    <section class="container v">
        <div class="row">
            <div class="col-12 col-md-3 navigator">
                <ul class="nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('admin')}}">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('analyticPage')}}">Statistiche</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('orderPage')}}">Ordini</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('userPage')}}">Utenti</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('editPage')}}">Menu</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="col-12">
                        <h3>Dashboard</h3>
                    </div>
                    <!--Sezione dei ordini-->
                    <div class="col-12 col-md-6">
                        <div class="info-box i-b-a" data-order-route="{{ route('getNOrder', ['n' => 10]) }}">
                            <div class="box-image"></div>
                            <div class="box-opacity-v"></div>
                            <div class="box-title">
                                <span class="fa fa-clock-o" aria-hidden="true"></span>
                                <span>Ordini</span>
                                <a class="link-v" href="{{route('orderPage')}}">Sfoglia tutti...</a>
                            </div>
                        </div><!--info-box-->
                    </div><!--col-12-->
                    <!--Sezione delle statistiche-->
                    <div class="col-12 col-md-6">
                        <div class="info-box">
                            <div class="box-image"></div>
                            <div class="box-opacity-v"></div>
                            <div class="box-title">
                                <span class="fa fa-line-chart" aria-hidden="true"></span>
                                <span>Statistiche</span>
                                <a class="link-v" href="{{route('analyticPage')}}">Sfoglia tutti...</a>
                            </div>
                            <div class="box-content-v">
                                <canvas id="myChart1" data-analytics-route="{{ route('getNAnalytic') }}"></canvas>
                            </div>
                        </div><!--info-box-->
                    </div><!--col-12-->
                </div><!--row2-->
            </div>
        </div><!--row-->
    </section>
</main>
@endsection

@section('footer')
@endsection