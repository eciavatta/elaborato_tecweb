@extends('layouts.app')
@section('title', 'Registrati - '.config('app.name', 'Cesena Food'))

@section('header')

@section('content')
<main class="main-container with-header">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-auto col-md-9">
                <header class="join-header">
                    @if ($step1)
                        <h2>Registrati su {{ config('app.name', 'Cesena Food') }}</h2>
                        <h3>Il modo migliore per ordinare del cibo quando vuoi.</h3>
                    @elseif ($step2)
                        <h2>Benvenuto su {{ config('app.name', 'Cesena Food') }}</h2>
                        <h3>Dicci qualcosa in più su di te, {{ session('name') }}</h3>
                    @elseif ($step3)
                        <h2>Benvenuto su {{ config('app.name', 'Cesena Food') }}</h2>
                        <h3>Ora sei pronto per ordinare quello che vuoi, {{ session('name') }}!</h3>
                    @endif
                </header>

                <div class="join-steps">
                    <div class="row">
                        @if ($step1)
                            <div class="col-sm col-md-4 padding-right-0">
                        @else
                            <div class="d-none d-md-block col-md-4 padding-right-0">
                        @endif
                            <div class="step with-border{{ $step1 ? ' current' : ' success' }}">
                                <div class="step-icon">
                                    <span class="fa fa-{{ $step1 ? 'user' : 'check' }}" aria-hidden="true"></span>
                                </div>
                                <div class="step-text">
                                    <div class="step-title">
                                        @if ($step1)
                                            Passo 1<span class="d-md-none">/3</span>:
                                        @else
                                            Completato
                                        @endif
                                    </div>
                                    <div class="step-description">{{ $step1 ? 'Crea account personale' : 'Creazione account' }}</div>
                                </div>
                            </div>
                        </div>

                        @if ($step2)
                            <div class="col-sm col-md-4 padding-0">
                        @else
                            <div class="d-none d-md-block col-md-4 padding-0">
                        @endif
                            <div class="step with-border with-padding{{ $step1 ? ' disabled' : ($step2 ? ' current' : ' success') }}">
                                <div class="step-icon">
                                    <span class="fa fa-{{ ($step1 or $step2) ? 'info-circle' : 'check' }}" aria-hidden="true"></span>
                                </div>
                                <div class="step-text">
                                    <div class="step-title">
                                        @if ($step1 or $step2)
                                            Passo 2<span class="d-md-none">/3</span>:
                                        @else
                                            Completato
                                        @endif
                                    </div>
                                    <div class="step-description">Ulteriori informazioni</div>
                                </div>
                            </div>
                        </div>

                        @if ($step3)
                            <div class="col-sm col-md-4 padding-0">
                        @else
                            <div class="d-none d-md-block col-md-4 padding-0">
                        @endif
                            <div class="step with-padding{{ $step3 ? ' current' : ' disabled' }}">
                                <div class="step-icon">
                                    <span class="fa fa-play-circle" aria-hidden="true"></span>
                                </div>
                                <div class="step-text">
                                    <div class="step-title">Passo 3<span class="d-md-none">/3</span>:</div>
                                    <div class="step-description">Account creato</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="join-form-container">
                    <div class="row">
                        <div class="col-sm col-md-6">
                            <header class="join-form-header">
                                @if ($step1)
                                    <h3>Crea il tuo account personale</h3>
                                @elseif ($step2)
                                    <h3>Inserisci informazioni aggiuntive</h3>
                                @elseif ($step3)
                                    <h3>Il tuo account è stato creato!</h3>
                                @endif
                            </header>

                            @if (!$errors->isEmpty())
                                <div class="join-form-errors">
                                    @component('components/alert', ['type' => 'danger'])
                                        @if ($errors->has('firstname'))
                                            {{ $errors->first('firstname') }}
                                        @elseif ($errors->has('lastname'))
                                            {{ $errors->first('lastname') }}
                                        @elseif ($errors->has('email'))
                                            {{ $errors->first('email') }}
                                        @elseif ($errors->has('password'))
                                            {{ $errors->first('password') }}
                                        @elseif ($errors->has('address'))
                                            {{ $errors->first('address') }}
                                        @elseif ($errors->has('telephone'))
                                            {{ $errors->first('telephone') }}
                                        @endif
                                    @endcomponent
                                </div>
                            @endif

                            <div class="join-form">
                                <form method="POST" action="{{ $step1 ? route('join') : ($step2 ? route('join.customize') : route('join.ready')) }}">
                                    {{ csrf_field() }}

                                    @if ($step1)
                                        <div class="form-group">
                                            <label for="firstname" class="control-label control-label-custom">Nome</label>
                                            <input id="firstname" type="text" class="form-control input-field-custom" name="firstname" value="{{ old('firstname') }}" required autofocus>
                                        </div>

                                        <div class="form-group">
                                            <label for="lastname" class="control-label control-label-custom">Cognome</label>
                                            <input id="lastname" type="text" class="form-control input-field-custom" name="lastname" value="{{ old('lastname') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="email" class="control-label control-label-custom">Indirizzo email</label>
                                            <input id="email" type="email" class="form-control input-field-custom" name="email" value="{{ old('email') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="password" class="control-label control-label-custom">Password</label>
                                            <input id="password" type="password" class="form-control input-field-custom" name="password" required>
                                        </div>

                                        <div class="policy-text">
                                            <hr>
                                            Cliccando su "Crea un account" qui sotto, dichiari di accettare i <a href="{{ route('terms-of-use') }}">Termini d'uso</a> e la <a href="{{ route('privacy-policy') }}">Privacy Policy</a>.
                                            <hr>
                                        </div>
                                    @elseif ($step2)
                                        <div class="form-group">
                                            <label for="address" class="control-label control-label-custom">Indirizzo</label>
                                            <input id="address" type="address" class="form-control input-field-custom" name="address" value="{{ old('address') }}" autofocus>
                                            <span class="help-block help-block-custom">Potrai inserire o modificare questo campo successivamente.</span>
                                        </div>

                                        <div class="form-group">
                                            <label for="telephone" class="control-label control-label-custom">Telefono</label>
                                            <input id="telephone" type="text" class="form-control input-field-custom" name="telephone" value="{{ old('telephone') }}">
                                            <span class="help-block help-block-custom">Potrai inserire o modificare questo campo successivamente.</span>
                                        </div>

                                        <div class="form-group">
                                            <label for="telegram_sync" class="control-label control-label-custom">Abilita notifiche su Telegram</label>
                                            <div style="margin-bottom: 50px">
                                                <a href="https://telegram.me/{{ config('telegram.bot_name') }}?start={{ session('telegram_token') }}" target="_blank" id="telegram_sync" class="btn btn-custom btn-custom-secondary-2 btn-telegram btn-full-mobile" data-sync-route="{{ route('telegram.is-sync') }}" role="button" aria-pressed="true">
                                                    <span class="fa fa-spinner fa-pulse fa-3x fa-fw d-none"></span>
                                                    <span class="text">Sincronizza account</span>
                                                </a>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group control-group">
                                        <button type="submit" class="btn btn-custom btn-custom-primary btn-full-mobile">{{ $step1 ? 'Crea un account' : ($step2 ? 'Continua' : 'Continua su '.config('app.name', 'Cesena Food')) }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="d-none d-md-block col-md-6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection
