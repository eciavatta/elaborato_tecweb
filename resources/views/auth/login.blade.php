@extends('layouts.app')
@section('title', 'Accedi - '.config('app.name', 'Cesena Food'))

@section('header')
@endsection

@section('content')
<main class="main-container login-wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-auto login-container">
                <header class="login-header">
                    <div class="logo">
                        <h1><a href="{{ route('home') }}">{{ config('app.name') }}</a></h1>
                    </div>
                    <div class="page-title">
                        <h2>Accedi a {{ config('app.name', 'Cesena Food') }}</h2>
                    </div>
                </header>

                @if (!$errors->isEmpty())
                    <section class="login-errors">
                        @component('components/alert', ['type' => 'danger'])
                            @if ($errors->has('email'))
                                {{ $errors->first('email') }}
                            @elseif ($errors->has('password'))
                                {{ $errors->first('password') }}
                            @endif
                        @endcomponent
                    </section>
                @endif

                <div class="login-form">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="control-label control-label-custom">Email address</label>
                            <input id="email" type="email" class="form-control input-field-custom" name="email" value="{{ old('email') }}" tabindex="1" required autofocus>
                        </div>

                        <div class="form-group">
                            <div class="password-label-container">
                                <label for="password" class="control-label control-label-custom" style="float: left;">Password</label>
                                <a style="float: right;" href="{{ route('password.request') }}" tabindex="4">Password dimenticata?</a>
                            </div>
                            <input id="password" type="password" class="form-control input-field-custom" name="password" tabindex="2" required>
                        </div>

                        <div class="form-group control-group">
                            <button type="submit" class="btn btn-custom btn-custom-primary btn-full" tabindex="3">Accedi</button>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom btn-custom-secondary-1 btn-full" disabled>Accedi con Unibo</button>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom btn-custom-secondary-2 btn-full" disabled>Accedi con Facebook</button>
                        </div>
                    </form>
                </div>

                <div class="register-redirect">
                    Nuovo su {{ config('app.name', 'Cesena Food') }}? <a href="{{ route('join') }}" tabindex="5">Crea un account.</a>
                </div>

                <footer class="login-footer">
                    <a style="margin-right: 4px;" href="{{ route('terms-of-use') }}">Termini d'uso</a>
                    <a style="margin-left: 4px; margin-right: 4px;" href="{{ route('privacy-policy') }}">Privacy Policy</a>
                    <a style="margin-left: 4px;" href="{{ route('contact-us') }}">Contattaci</a>
                </footer>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection
