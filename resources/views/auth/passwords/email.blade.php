@extends('layouts.app')
@section('title', 'Resetta password - '.config('app.name', 'Cesena Food'))

@section('header')
@endsection

@section('content')
<main class="main-container login-wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-auto login-container">
                <header class="login-header">
                    <div class="logo">
                        <h1><a href="{{ route('home') }}">{{ config('app.name') }}</a></h1>
                    </div>
                    <div class="page-title">
                        <h2>Resetta la tua password</h2>
                    </div>
                </header>

                <div class="login-errors">
                    @if (session('success'))
                        @component('components/alert', ['type' => 'success'])
                            {{ session('success') }}
                        @endcomponent
                    @endif

                    @if (session('error') or !$errors->isEmpty())
                        @component('components/alert', ['type' => 'danger'])
                            @if (session('error'))
                                {{ session('error') }}
                            @elseif ($errors->has('email'))
                                {{ $errors->first('email') }}
                            @endif
                        @endcomponent
                    @endif
                </div>

                <div class="login-form">
                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="control-label control-label-custom">Email address</label>
                            <input id="email" type="email" class="form-control input-field-custom" name="email" value="{{ old('email') }}" required autofocus>
                        </div>

                        <div class="form-group control-group">
                            <button type="submit" class="btn btn-custom btn-custom-primary btn-full">Invia email di reset della password</button>
                        </div>
                    </form>
                </div>

                <footer class="login-footer">
                    <a style="margin-right: 4px;" href="{{ route('terms-of-use') }}">Termini d'uso</a>
                    <a style="margin-left: 4px; margin-right: 4px;" href="{{ route('privacy-policy') }}">Privacy Policy</a>
                    <a style="margin-left: 4px;" href="{{ route('contact-us') }}">Contattaci</a>
                </footer>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection
