@extends('layouts.app')
@section('title', 'Resetta password - '.config('app.name', 'Cesena Food'))

@section('header')
@endsection

@section('content')
<main class="main-container login-wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-auto login-container">
                <header class="login-header">
                    <div class="logo">
                        <h1><a href="{{ route('home') }}">{{ config('app.name') }}</a></h1>
                    </div>
                    <div class="page-title">
                        <h2>Resetta password</h2>
                    </div>
                </header>

                <div class="login-alerts">
                    @if (session('status'))
                        @component('components/alert', ['type' => 'success'])
                            {{ session('success') }}
                        @endcomponent
                    @endif

                    @if (!$errors->isEmpty())
                        @component('components/alert', ['type' => 'danger'])
                            @if ($errors->has('token'))
                                {{ $errors->first('token') }}
                            @elseif ($errors->has('password'))
                                {{ $errors->first('password') }}
                            @endif
                        @endcomponent
                    @endif
                </div>

                <div class="login-form">
                    <form method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="password" class="control-label control-label-custom">Password</label>
                            <input id="password" type="password" class="form-control input-field-custom" name="password" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="control-label control-label-custom">Conferma Password</label>
                            <input id="password-confirm" type="password" class="form-control input-field-custom" name="password_confirmation" required>
                        </div>

                        <div class="form-group control-group">
                            <button type="submit" class="btn btn-custom btn-custom-primary btn-full">Cambia password</button>
                        </div>
                    </form>
                </div>

                <footer class="login-footer">
                    <a style="margin-right: 4px;" href="{{ route('terms-of-use') }}">Termini d'uso</a>
                    <a style="margin-left: 4px; margin-right: 4px;" href="{{ route('privacy-policy') }}">Privacy Policy</a>
                    <a style="margin-left: 4px;" href="{{ route('contact-us') }}">Contattaci</a>
                </footer>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection

