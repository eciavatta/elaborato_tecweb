@extends('layouts.app')
@section('title', config('app.name', 'Cesena Food').' - Page not found')

@section('content')
<main class="main-container with-header">
    <section class="error-wrapper">
        <div class="container">
            <div class="row align-items-center justify-content-center error-container">
                <div class="col-md-6">
                    <div class="error-text">
                        <h3 class="error-title">Sembra che manchi qualcosa</h3>
                        <p class="error-message">Non siamo riusciti a preparare la pagina che stavi cercando perché non esiste.</p>
                        <p class="error-message">Ritorna alla <a href="{{ route('home') }}">homepage</a>.</p>
                        <p class="error-code">Codice di errore: 404</p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="error-image">
                        <img src="{{ asset('vectors/error404.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
