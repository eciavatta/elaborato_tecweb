@extends('layouts.app')
@section('title', config('app.name', 'Cesena Food'))

@section('content')
<main class="main-container with-header">
    <header class="image-wrapper">
        <div class="container">
            <div class="main-title">
                <h2 class="title">Ordina ora il tuo pasto</h2>
                <h3 class="motto">Consegna in pochi minuti in tutta Cesena</h3>
            </div>
            <div class="menu-button">
                <a href="{{ route('menu') }}" class="btn btn-custom btn-custom-primary">Sfoglia menù</a>
            </div>
        </div>
    </header>

    <section class="box-wrapper">
        <div class="container">
            <h3 class="section-title">Come fare?</h3>
            <div class="section-description">Ordinare su CesenaFood è molto semplice!</div>

            <div class="row box-container">
                <div class="col-12 col-md-4">
                    <div class="box box-type-one">
                        <div class="box-image primary">
                            <img src="{{ asset('vectors/hamburger.svg') }}" class="howto1" alt="">
                        </div>
                        <div class="box-content">
                            <h4 class="title">Scopri</h4>
                            <div>Sfoglia il listino e cerca il tuo cibo preferito.</div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="box box-type-one">
                        <div class="box-image secondary-1">
                            <img src="{{ asset('vectors/hotdog.svg') }}" class="howto2" alt="">
                        </div>
                        <div class="box-content">
                            <h4 class="title">Ordina</h4>
                            <div>Vai alla cassa virtuale e effettua l'ordine.</div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="box box-type-one">
                        <div class="box-image secondary-2">
                            <img src="{{ asset('vectors/pizza.svg') }}" class="howto3" alt="">
                        </div>
                        <div class="box-content">
                            <h4 class="title">Ricevi</h4>
                            <div>Aspetta alla porta che arrivi il tuo cibo!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="box-wrapper background-one">
        <div class="container-fluid">
            <h3 class="section-title">Informazioni</h3>

            <div class="row justify-content-center">
                <div class="col-auto">
                    <div class="box box-type-two">
                        <div class="box-content">
                            <div class="title">Orari</div>
                            <div>Lunedì - Venerdì</div>
                            <div style="margin-bottom: 10px;">11:00 - 22:00</div>
                            <div>Sabato e Domenica</div>
                            <div>18:00 - 22:00</div>
                        </div>

                        <div class="box-image">
                            <img src="{{ asset('vectors/clock.svg') }}" alt="">
                        </div>
                    </div>
                </div>

                <div class="col-auto">
                    <div class="box box-type-two">
                        <div class="box-image">
                            <img src="{{ asset('vectors/details.svg') }}" alt="">
                        </div>

                        <div class="box-content">
                            <div class="title">Dettagli</div>
                            <div>Nessun costo di consegna</div>
                            <div>Minimo 15 € per spedizione</div>
                            <div class="icons">
                                <span class="fa fa-money" aria-hidden="true"></span>
                                <span class="fa fa-credit-card" aria-hidden="true"></span>
                                <span class="fa fa-paypal" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-auto">
                    <div class="box box-type-two">
                        <div class="box-content">
                            <div class="title">Dove siamo</div>
                            <div>Siamo a Cesena in</div>
                            <div style="margin-bottom: 10px;">Via Giovanni Giolitti, 27</div>
                            <div>Numero di telefono</div>
                            <div>0547 000000</div>
                        </div>

                        <div class="box-image">
                            <img src="{{ asset('vectors/world.svg') }}" alt="">
                        </div>
                    </div>
                </div>

                <div class="col-auto">
                    <div class="box box-type-two">
                        <div class="box-image">
                            <img src="{{ asset('vectors/contact.svg') }}" alt="">
                        </div>

                        <div class="box-content">
                            <div class="title">Scrivici</div>
                            <div>Invia una mail a</div>
                            <div><a href="mailto:cesenafood@ingenziati.eu">cesenafood@ingenziati.eu</a></div>
                            <div class="icons">
                                <span class="fa fa-facebook-official" aria-hidden="true"></span>
                                <span class="fa fa-twitter" aria-hidden="true"></span>
                                <span class="fa fa-university" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="box-wrapper">
        <div class="container">
            <h3 class="section-title">Perché ordinare su Cesena Food?</h3>

            <div class="row box-container">
                <div class="col-12 col-md-4">
                    <div class="box box-type-one">
                        <div class="box-image primary">
                            <img src="{{ asset('vectors/toast.svg') }}" class="why1" alt="">
                        </div>
                        <div class="box-content">
                            <h4 class="title">Ampia scelta</h4>
                            <div>Il nostro listino contiene una vasta gamma di prodotti.</div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="box box-type-one">
                        <div class="box-image secondary-1">
                            <img src="{{ asset('vectors/chicken.svg') }}" class="why2" alt="">
                        </div>
                        <div class="box-content">
                            <h4 class="title">Qualità</h4>
                            <div>Tutti i prodotti sono realizzati da noi con ingredienti di prima qualità.</div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="box box-type-one">
                        <div class="box-image secondary-2">
                            <img src="{{ asset('vectors/cake.svg') }}" class="why3" alt="">
                        </div>
                        <div class="box-content">
                            <h4 class="title">Comodità</h4>
                            <div>Ricevi il tuo piatto preferito in poco tempo direttamente a casa tua!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="box-wrapper background-two">
        <div class="container">
            <div class="row justify-content-center box-container">
                <div class="col-12 col-md-6">
                    <div class="box">
                        <div class="box-title" style="margin-bottom: 20px">
                            <h3 class="section-title">A proposito di Cesena Food</h3>
                        </div>

                        <div class="box-content">
                            Cesena Food è un portale creato come progetto del corso di Tecnologie Web dagli studenti del corso di laurea in Ingengneria e Scienze Informatiche Emiliano Ciavatta &lt;<a href="mailto:emiliano.ciavatta@studio.unibo.it">emiliano.ciavatta@studio.unibo.it</a>&gt; e Simone Golinucci &lt;<a href="simone.golinucci@studio.unibo.it">simone.golinucci@studio.unibo.it</a>&gt;
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="box box-type-one">
                        <div class="box-title" style="margin-bottom: 20px">
                            <h3 class="section-title">Dicono di noi</h3>
                        </div>

                        <div class="box-image" style="height: 60px;">
                            <img src="{{ asset('vectors/user.svg') }}" style="width: 60px;" alt="">
                        </div>

                        <div class="box-content">
                            Testo lungo abbastanza convincente ma non troppo.
                            <div><strong>@angela</strong></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
