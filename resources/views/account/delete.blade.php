@extends('layouts.app')
@section('title', 'Elimina account - '.config('app.name', 'Cesena Food'))

@section('header')
@endsection

@section('content')
<main class="main-container login-wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-auto login-container">
                <header class="login-header">
                    <div class="logo">
                        <h1><a href="{{ route('home') }}">{{ config('app.name') }}</a></h1>
                    </div>
                    <div class="page-title">
                        <h2>Elimina account</h2>
                    </div>
                </header>

                <div class="login-alerts">
                    @if (!$errors->isEmpty())
                        @component('components/alert', ['type' => 'danger'])
                            @if ($errors->has('password'))
                                {{ $errors->first('password') }}
                            @elseif ($errors->has('verify'))
                                {{ $errors->first('verify') }}
                            @endif
                        @endcomponent
                    @endif
                </div>

                <div class="login-form">
                    <form method="POST" action="{{ route('account.delete') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            Attenzione, questo modulo è <strong>estremamente importante</strong>. Effettuando l'eliminazione dell'account procederemo immediatamente a cancellare tutti i tuoi dati dal nostro sistema. Procedi con cautela.
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label control-label-custom">Password</label>
                            <input id="password" type="password" class="form-control input-field-custom" name="password" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="verify" class="control-label control-label-custom">Per verificare, scrivi <em>{{ trans('account.delete_command') }}</em> qui sotto.</label>
                            <input id="verify" type="text" class="form-control input-field-custom" name="verify" required>
                        </div>

                        <div class="form-group control-group">
                            <button type="submit" class="btn btn-custom btn-custom-primary btn-full">Cancella questo account</button>
                        </div>
                    </form>
                </div>

                <footer class="login-footer">
                    <a style="margin-right: 4px;" href="{{ route('terms-of-use') }}">Termini d'uso</a>
                    <a style="margin-left: 4px; margin-right: 4px;" href="{{ route('privacy-policy') }}">Privacy Policy</a>
                    <a style="margin-left: 4px;" href="{{ route('contact-us') }}">Contattaci</a>
                </footer>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection

