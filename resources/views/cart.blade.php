@extends('layouts.app')
@section('title', 'Carrello - '.config('app.name', 'Cesena Food'))

@section('content')
<main class="main-container with-header">
    <div class="menu-wrapper">
        <header class="page-title">
            <h2 class="title">Carrello</h2>
            <div class="description">Controlla la lista prima di ordinare</div>
        </header>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-md-6 col-lg-4">
                    <div class="menu-block">
                        <div class="menu-title">
                            <h3 class="title">
                                <span class="fa fa-shopping-cart" aria-hidden="true"></span>
                                Riepilogo ordine
                            </h3>
                        </div>

                        <div class="cart-summary">
                            @component('cart/summary', ['totalPrice' => $totalPrice, 'locked' => $locked])
                            @endcomponent
                        </div>

                        <div class="cart-cancel">
                            <a href="{{ route('menu') }}" class="btn btn-custom btn-custom-secondary-2 btn-full">Aggiungi più articoli</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
@endsection
