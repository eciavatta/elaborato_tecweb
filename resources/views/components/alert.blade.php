@if ($type)
    <div class="alert alert-custom-{{ $type }} alert-dismissible fade show" role="alert">
@else
    <div class="alert alert-dismissible fade show" role="alert">
@endif
    {{ $slot }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Chiudi">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
