'use strict'

const path = require('path')
const babel = require('rollup-plugin-babel')
const resolve = require('rollup-plugin-node-resolve')
const pkg = require(path.resolve(__dirname, '../package.json'))

let fileDest = 'script.js'
const external = ['chart.js', 'jquery']
const plugins = [
  babel({
    exclude: 'node_modules/**', // only transpile our source code
    externalHelpersWhitelist: [ // include only required helpers
      'defineProperties',
      'createClass',
      'inheritsLoose',
      'extends'
    ]
  })
]
const globals = {
  'chart.js': 'Chart',
  'jquery': 'jQuery' // ensure we use jQuery which is always available even in noConflict mode
}

module.exports = {
  input: path.resolve(__dirname, '../resources/assets/js/index.js'),
  output: {
    file: path.resolve(__dirname, `../public/js/${fileDest}`),
    format: 'umd'
  },
  name: 'CesenaFood',
  external,
  globals,
  plugins,
  banner: `/*!
 * CesenaFood v` + process.env.npm_package_version + `
 * Written by Emiliano Ciavatta and Simone Golinucci
 * This work is licensed under a Creative Commons Attribution 4.0 International License
 * <https://creativecommons.org/licenses/by/4.0/>
 */`
}
