# Cesena Food [![Build Status](https://circleci.com/bb/eciavatta/elaborato_tecweb.svg?style=shield)](https://circleci.com/bb/eciavatta/elaborato_tecweb)

## Installazione

- Scaricare il repository con il comando `git clone`
- Eseguire il comando `composer install` per installare le dipendenze di backend
- Eseguire il comando `npm install` per installare le dipendenze di frontend

## Aggiornamento

- Per aggiornare le dipendenze dei package backend utilizzare il comando `composer update`
- Per aggiornare le dipendenze dei package frontend utilizzare il comando `npm update`

## Partecipanti

Questo progetto è stato realizzato da:

- Emiliano Ciavatta <[emiliano.ciavatta@studio.unibo.it](mailto:emiliano.ciavatta@studio.unibo.it)>
- Simone Golinucci <[simone.golinucci@studio.unibo.it](mailto:simone.golinucci@studio.unibo.it)>

## Licenza

Quest'opera è distribuita con [Licenza Creative Commons Attribuzione 4.0 Internazionale](https://creativecommons.org/licenses/by/4.0/)

## Comandi
- Rigenera autoloader: `composer dump-autoload`
- Cancella tabelle e ricreale, con seed: `php artisan migrate:fresh --seed`

## Generazione contenuti
- Crea factory: `php artisan make:factory NameFactory`
- Crea migration: `php artisan make:migration create_tablename_table -create=tablename`
- Genera seeder: `php artisan make:seeder NameTableSeeder`
