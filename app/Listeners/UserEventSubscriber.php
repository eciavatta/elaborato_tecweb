<?php

namespace App\Listeners;

use Illuminate\Http\Request;

class UserEventSubscriber
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function onUserRegistered($event) {
        $this->logEvent($event->user, 'register');
    }

    public function onUserLogin($event) {
        $this->logEvent($event->user, 'login_success');
    }

    public function onUserFailed($event) {
        if ($event->user) {
            $this->logEvent($event->user, 'login_failed');
        }
    }

    public function onUserLogout($event) {
        if ($event->user) {
            $this->logEvent($event->user, 'logout');
        }
    }

    public function onUserLockout($event) {
        if ($event->user) {
            $this->logEvent($event->user, 'lockout');
        }
    }

    public function onUserPasswordReset($event) {
        $this->logEvent($event->user, 'password_reset');
    }

    private function logEvent($user, $action) {
        $user->actions()->create([
            'action' => $action,
            'ip_address' => $this->request->ip(),
            'user_agent' => $this->request->userAgent(),
        ]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'App\Listeners\UserEventSubscriber@onUserRegistered'
        );

        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Failed',
            'App\Listeners\UserEventSubscriber@onUserFailed'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );

        $events->listen(
            'Illuminate\Auth\Events\Lockout',
            'App\Listeners\UserEventSubscriber@onUserLockout'
        );

        $events->listen(
            'Illuminate\Auth\Events\PasswordReset',
            'App\Listeners\UserEventSubscriber@onUserPasswordReset'
        );
    }
}
