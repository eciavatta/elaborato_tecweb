<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class UserJoined extends Notification
{
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(__('notifications.user_joined_subject', ['title' => config('app.name')]))
                    ->greeting(__('notifications.user_joined_greeting', ['name' => $notifiable->firstname]))
                    ->line(__('notifications.user_joined_line1', ['title' => config('app.name')]))
                    ->line(__('notifications.user_joined_line2'))
                    ->action(__('notifications.user_joined_action'), url('/menu'))
                    ->line(__('notifications.user_joined_line3'));
    }
}
