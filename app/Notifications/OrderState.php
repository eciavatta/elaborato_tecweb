<?php

namespace App\Notifications;

use App\Telegram\TelegramChannel;
use App\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\SimpleMessage;
use Illuminate\Notifications\Messages\MailMessage;

class OrderState extends Notification
{
    public function __construct($state)
    {
        $this->orderState = $state;
        $this->str = '';
        $this->val = '';
        switch($this->orderState){
            case 'processing':
                $this->val = 'è in fase di lavorazione';
                $this->str = 'notifications.order_state_wait_1';
            break;
            case 'sent':
                $this->val = 'è stato spedito';
                $this->str = 'notifications.order_state_wait_2';
            break;
            case 'delivered':
                $this->val = 'è stato consegnato';
                $this->str = 'notifications.order_state_wait_3';
            break;
        }
    }
    public function via($notifiable)
    {
        return $notifiable->telegram_id ? [TelegramChannel::class, 'mail'] : ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(__('notifications.order_state_subject', ['title' => config('app.name')]))
                    ->greeting(__('notifications.order_state_greeting', ['name' => $notifiable->firstname]))
                    ->line(__('notifications.order_state_line1', ['state' => $this->val]))
                    ->line(__($this->str))
                    ->action(__('notifications.order_state_action'), url('/menu'));
    }

    public function toTelegram($notifiable)
    {
        $simpleMessage = (new SimpleMessage)
                    ->subject(__('notifications.order_state_subject', ['title' => config('app.name')]))
                    ->line(__('notifications.order_state_greeting', ['name' => $notifiable->firstname]))
                    ->line(__('notifications.order_state_line1', ['state' => $this->val]))
                    ->line(__($this->str));

        return $simpleMessage;
    }
}
