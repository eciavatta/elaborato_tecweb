<?php

namespace App\Notifications;

use App\Telegram\TelegramChannel;
use App\Telegram\TelegramMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\SimpleMessage;
use Illuminate\Notifications\Messages\MailMessage;

class OrderCreated extends Notification
{
    public function __construct($order)
    {
        $this->order = $order;
    }

    public function via($notifiable)
    {
        return $notifiable->telegram_id ? [TelegramChannel::class, 'mail'] : ['mail'];
    }

    public function toMail($notifiable)
    {
        $details = $this->order->dishes()->get();

        $mailMessage = (new MailMessage)
                    ->subject(__('notifications.order_created_subject'))
                    ->greeting(__('notifications.order_created_greeting'));
        if ($this->order->address) {
            $mailMessage->line(__('notifications.order_created_line1_delivery', ['time' => $this->order->time]));
        } else {
            $mailMessage->line(__('notifications.order_created_line1_pickup', ['time' => $this->order->time]));
        }
        $mailMessage->line(__('notifications.order_created_line2'));

        foreach ($details as $dish) {
            $mailMessage->line('&emsp;&emsp;' . $dish['pivot']['quantity'] . ' x '.$dish['name'] . ' (&euro; ' .
                            number_format($dish['pivot']['single_price'] * $dish['pivot']['quantity'] / 100, 2).')');
        }

        $totalPrice = $details->reduce(function ($carry, $item) {
            return $carry + ($item['pivot']['single_price'] * $item['pivot']['quantity']);
        });

        $mailMessage->line(__('notifications.order_created_line3', ['price' => number_format($totalPrice / 100, 2)]));
        $mailMessage->line(__('notifications.order_created_line4'));

        return $mailMessage;
    }

    public function toTelegram($notifiable)
    {
        $simpleMessage = (new SimpleMessage)
                    ->subject(__('notifications.order_created_subject'))
                    ->line(__('notifications.order_created_greeting'));
        if ($this->order->address) {
            $simpleMessage->line(__('notifications.order_created_line1_delivery', ['time' => $this->order->time]));
        } else {
            $simpleMessage->line(__('notifications.order_created_line1_pickup', ['time' => $this->order->time]));
        }
        $simpleMessage->line(__('notifications.order_created_line4'));

        return $simpleMessage;
    }
}
