<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'time', 'note', 'state', 'paid',
    ];

    /**
     * Get the user that made the order.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The dishes that belong to the order.
     */
    public function dishes()
    {
        return $this->belongsToMany('App\Dish', 'order_details')->withPivot('quantity', 'single_price');
    }
}
