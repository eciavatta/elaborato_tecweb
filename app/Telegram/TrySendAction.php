<?php

namespace App\Telegram;

use Closure;
use Telegram\Bot\Exceptions\TelegramResponseException;

trait TrySendAction
{
    private function trySendAction(Closure $action)
    {
        try {
            $action();
        } catch (TelegramResponseException $e) {
            //
        }
    }
}
