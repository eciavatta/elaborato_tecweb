<?php

namespace App\Telegram;

use Illuminate\Notifications\Notification;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramChannel
{
    use ComposeResponse, TrySendAction;

    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toTelegram($notifiable);
        $this->trySendAction(function() use ($message, $notifiable) {
            $params = collect($this->composeResponse($message))->put('chat_id', $notifiable->telegram_id);
            $response = Telegram::sendMessage($params->all());
        });
    }
}
