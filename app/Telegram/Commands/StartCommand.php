<?php

namespace App\Telegram\Commands;

use App\Telegram\TelegramCommand;
use App\User;
use Illuminate\Notifications\Messages\SimpleMessage;
use Illuminate\Support\Facades\Cache;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends TelegramCommand
{
    protected $name = "start";

    protected $description = "Abilita le notifiche su Telegram";

    public function handle($arguments)
    {
        $this->startTyping();
        $telegramId = $this->update['message']['from']['id'];
        $user = User::where('telegram_id', '=', $telegramId)->first();
        $telegramToken = trim($arguments);

        if ($user) {
            $message = (new SimpleMessage)
                            ->subject(__('telegram.already_sync_subject'))
                            ->line(__('telegram.already_sync_line1', ['email' => $user->email]))
                            ->line(__('telegram.already_sync_line2'))
                            ->line(__('telegram.already_sync_line3'));
            return $this->sendMessage($message);
        }

        if (empty($telegramToken) or !Cache::has($telegramToken)) {
            $message = (new SimpleMessage)
                            ->subject(__('telegram.invalid_token_subject'))
                            ->line(__('telegram.invalid_token_line1', ['title' => config('app.name')]))
                            ->action(__('telegram.invalid_token_action'), config('app.deploy_url').'/profile');
            return $this->sendMessage($message);
        }

        $user = Cache::pull($telegramToken);
        $user->telegram_id = $telegramId;
        $user->save();

        $message = (new SimpleMessage)
                            ->subject(__('telegram.sync_success_subject'))
                            ->line(__('telegram.sync_success_line1', ['email' => $user->email]))
                            ->line(__('telegram.sync_success_line2'))
                            ->line(__('telegram.sync_success_line3'));
        return $this->sendMessage($message);
    }
}
