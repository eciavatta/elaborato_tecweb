<?php

namespace App\Telegram;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

abstract class TelegramCommand extends Command
{
    use ComposeResponse, TrySendAction;

    protected function startTyping()
    {
        $this->trySendAction(function() {
            $this->replyWithChatAction(['action' => Actions::TYPING]);
        });
    }

    protected function sendMessage($message)
    {
        $this->trySendAction(function() use ($message) {
            $params = $this->composeResponse($message);
            $this->replyWithMessage($params);
        });
    }
}
