<?php

namespace App\Telegram;

trait ComposeResponse
{
    private function composeResponse($message) {
        $params = collect([
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true,
            'disable_notification' => false
        ]);

        $text = '';
        if ($message->subject) {
            $text .= sprintf("*%s*\n", $message->subject);
        }
        $text .= implode("\n", $message->introLines);
        $text .= implode("\n", $message->outroLines);
        $params->put('text', $text);

        if ($message->actionText) {
            $inline_keyboard = [
                'inline_keyboard' => [[[
                    'text' => $message->actionText,
                    'url' => $message->actionUrl
                ]]]
            ];

            $reply_markup = json_encode($inline_keyboard);
            $params->put('reply_markup', $reply_markup);
        }

        return $params->all();
    }
}
