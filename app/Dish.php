<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dish extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'price', 'order',
    ];

    /**
     * Get the category that owns the dish.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * The user favourites that belong to the dish.
     */
    public function favouritesFor()
    {
        return $this->belongsToMany('App\User', 'favourites');
    }

    /**
     * The orders that belong to the dish.
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order', 'order_details')->withPivot('quantity', 'single_price');
    }
}
