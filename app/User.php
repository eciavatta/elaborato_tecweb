<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The dishes that user prefers.
     */
    public function favouriteDishes()
    {
        return $this->belongsToMany('App\Dish', 'favourites');
    }

    /**
     * Get the orders of the user.
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Get the dishes for the category.
     */
    public function actions()
    {
        return $this->hasMany('App\UserAction');
    }
}
