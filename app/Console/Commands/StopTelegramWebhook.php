<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class StopTelegramWebhook extends Command
{
    protected $signature = 'telegram:stop';

    protected $description = 'Stop receiving updates via webhook from Telegram server';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $response = Telegram::removeWebhook();
        if ($response->isError()) {
            $this->error(json_encode($response->getDecodedBody()));
        } else {
            $this->info(json_encode($response->getDecodedBody()));
        }
    }
}
