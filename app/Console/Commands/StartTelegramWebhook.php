<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartTelegramWebhook extends Command
{
    protected $signature = 'telegram:start';

    protected $description = 'Start receiving updates via webhook from Telegram server';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $response = Telegram::setWebhook(['url' => url(config('telegram.bot_token').'/webhook')]);
        $result = [
            'ok' => $response[0],
            'result' => true,
            'description' => ''
        ];

        if (!$result['ok']) {
            $this->error(json_encode($result));
        } else {
            $this->info(json_encode($result));
        }
    }
}
