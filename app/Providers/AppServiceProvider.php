<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('env', function ($environment) {
            return app()->environment($environment);
        });

        Blade::if('route', function ($route) {
            return collect(explode('|', $route))->reduce(function ($carry, $item) {
                return $carry or Request::is($item);
            });
        });

        Blade::directive('version', function () {
            if (app()->environment('production')) {
                return "<?php echo '?v=' . config('app.version', '1.0.0'); ?>";
            } else {
                return "<?php echo '?v=' . rand(); ?>";
            }

        });

        Blade::if('debug', function () {
            return config('app.debug', false);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
