<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\CalculateTotalPrice;
use App\Notifications\OrderCreated;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    use CalculateTotalPrice;

    public function __construct()
    {
        $this->middleware(['auth', 'order']);
    }

    public function showStepOne()
    {
        return view('order', ['step1' => true, 'step2' => false,'step3' => false,
                              'totalPrice' => $this->getTotalPrice()]);

    }

    public function showStepTwo(Request $request)
    {
        if ($request->session()->has('stepTwo')) {
            return view('order', ['step1' => false, 'step2' => true,'step3' => false,
                                  'totalPrice' => $this->getTotalPrice()]);
        } else {
            return redirect()->route('menu');
        }
    }

    public function showStepThree(Request $request)
    {
        if ($request->session()->has('stepThree')) {
            return view('order', ['step1' => false, 'step2' => false,'step3' => true,
                                  'totalPrice' => $this->getTotalPrice()]);
        } else {
            return redirect()->route('menu');
        }
    }

    public function stepOne(Request $request)
    {
        if (session()->get('delivery')) {
            $this->validate($request, [
                'address' => 'required|string|max:256',
            ]);
        }

        $this->validate($request, [
            'time' => 'required|date_format:H:i',
            'note' => 'nullable|string|max:256',
        ]);

        session()->put('stepTwo', true);
        session()->put('address', session()->get('delivery') ? $request->get('address') : null);
        session()->put('time', $request->get('time'));
        session()->put('note', $request->get('note'));

        return redirect()->route('order.payments');
    }

    public function stepTwo(Request $request)
    {
        if (!session()->has('stepTwo')) {
            return redirect()->route('menu');
        }

        $this->validate($request, [
            'payment' => [
                'required',
                Rule::in(['cash', 'creditCard', 'paypal']),
            ],
        ]);

        $order = Auth::user()->orders()->create([
            'address' => session()->pull('address', null),
            'time' => session()->pull('time', null),
            'note' => session()->pull('note', null),
            'state' => 'received',
            'paid' => true,
        ]);

        $details = session()->get('cart')->map(function ($item, $key) {
            return [
                'quantity' => $item['quantity'],
                'single_price' => $item['price'],
            ];
        });
        $order->dishes()->attach($details);

        Auth::user()->notify(new OrderCreated($order));

        session()->forget('cart');
        session()->forget('stepTwo');
        session()->put('stepThree', true);

        return redirect()->route('order.complete');
    }

    public function stepThree(Request $request)
    {
        if (!session()->has('stepThree')) {
            return redirect()->route('menu');
        }

        session()->forget('buying');
        session()->forget('delivery');
        session()->forget('stepThree');
        return redirect()->route('home');
    }
}
