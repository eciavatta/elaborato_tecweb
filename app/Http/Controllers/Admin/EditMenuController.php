<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;
use App\Dish;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EditMenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Show the client order.
     *
     * @return \Illuminate\Http\Response
     *
     * Carica la pagina con tutti i dati neccessari per gli ordini,
     * inoltre la pagina è generata da php
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.editMenu', ['categories' => $categories]);
    }

    public function newDish(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'categoryId' => 'required|integer',
            'price' => 'required|numeric',
        ]);
        $dishOrder = count(Dish::all()->where('category_id', $request->get('categoryId')));
        DB::table('dishes')->insert([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'category_id' => $request->get('categoryId'),
            'available'=> 1,
            'order' => $dishOrder,
            'price' => $request->get('price')*100,
            'created_at' => date('Y-m-d H:i:s')
            ]);
        return 1;
    }
    public function deleteDish(Request $request)
    {
        $request->validate([
            'id' => 'required|integer'
        ]);
        DB::table('dishes')
            ->where('id', '=', $request->get('id'))
            ->update(['available'=> 0, 'updated_at' => date('Y-m-d H:i:s')]);
        return 1;
    }
    public function updatePrice(Request $request)
    {
        $request->validate([
            'id' => 'required|integer',
            'price' => 'required|numeric'
        ]);
        DB::table('dishes')
        ->where('id', $request->get('id'))
        ->update(['price' => $request->get('price')*100, 'updated_at' => date('Y-m-d H:i:s')]);
        return 1;
    }
    public function newCategory(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);
        $categoryOrder = count(Category::all());
        DB::table('categories')->insert([
            'name' => $request->get('name'),
            'active'=> 1,
            'order' => $categoryOrder,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return 1;
    }
    public function deleteCategory(Request $request)
    {
        $request->validate([
            'id' => 'required|integer'
        ]);
        DB::table('categories')
            ->where('id', '=', $request->get('id'))
            ->update(['active'=> 0, 'updated_at' => date('Y-m-d H:i:s')]);
        return 1;
    }
}
