<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\OrderState;
use App\Order;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Show the client order.
     *
     * @return \Illuminate\Http\Response
     *
     * Carica la pagina con tutti i dati neccessari per gli ordini,
     * inoltre la pagina è generata da php
     */
    public function index()
    {
        $this->getAllOrders();
        return view('admin.order');
    }
    /**
     * this function get all the orders
     *
     * @return \Illuminate\Http\Response
     *
     * restituisce n ordini in formato json richiamabile da ajax
     *
     */
    public function getAllOrders()
    {
        /* Magari aggiungere anche i piatti presenti nell'ordine*/
        $orders = Order::all()
            ->where('created_at', '>', date('Y-m-d 00-00-00'));
        $orders->map(function ($order){
            return $order->dishes;
        });
        $orders->map(function ($order){
            return $order->user;
        });
        $orders->map(function ($order){
            $order->update_time = $order->updated_at->format('H:i:s');
            return $order;
        });
        session()->put('orders', $orders);
    }
    /**
     * this function get n the orders
     *
     * @return \Illuminate\Http\Response
     *
     * restituisce n ordini in formato json richiamabile da ajax
     *
     */
    public function getOrder($n)
    {
        /* Magari aggiungere anche i piatti presenti nell'ordine*/
        $orders = Order::all()
            ->where('created_at', '>', date('Y-m-d 00-00-00'));


        return $orders;
        $orders = $orders->sortByDesc('created_at');
        return $orders->take($n);
    }

    public function updateOrder(Request $request){
        $request->validate([
            'id' => 'required|integer'
        ]);
        $id = $request->get('id');
        $state = ['received', 'processing', 'sent', 'delivered'];
        $order = Order::all()->where('id', $id)->first();
        $user = User::all()->where('id', $order->user_id)->first();
        if ($order->state == $state[0]) {
            $nextState = 'processing';
        } elseif ($order->state == $state[1]) {
            $nextState = 'sent';
        } else {
            $nextState = 'delivered';
        }
        DB::table('orders')
            ->where('id', $id)
            ->update(['state' => $nextState]);
        $user->notify(new OrderState($nextState));
    }
}
