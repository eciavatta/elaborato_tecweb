<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Show the client order.
     *
     * @return \Illuminate\Http\Response
     *
     * Carica la pagina con tutti i dati neccessari per gli ordini,
     * inoltre la pagina è generata da php
     */
    public function index()
    {
        $this->getUser();
        return view('admin.user');
    }
    
    public function getUser()
    {
        $user = User::all();
        session()->put('utenti', $user);
    }
}