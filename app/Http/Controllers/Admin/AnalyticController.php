<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;
use App\Dish;
use App\Order;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AnalyticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Show the analytic page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.analytic');
    }

    /**
     * Show the analytic.
     *
     * @return \Illuminate\Http\Response
     *
     * come valore di ritorno bisogna ritornare un qualsiasi cosa che
     * convertito in json abbia la seguente struttura:
     *      {"YYYY-MM-DD": value,...,"YYYY-MM-DD": value}
     * dove:
     *      YYYY-MM-DD; è il giorno a cui è riferita la statistica,
     *      value; è il valore a cui si riferisce la statistica.
     */
    public function getAnalytic(Request $request)
    {
        $this->validate($request, [
            'ascisse' => 'required|integer',
            'ordinate' => 'required|integer',
            'numberOfDay' => 'required|integer'
        ]);
        $minDate = date('Y-m-d 00-00-00', strtotime(-$request->get('numberOfDay').' day'));
        /* Prendo tutti i dati */
        $orders = Order::all()->where('created_at', '>', $minDate)->sortBy('created_at');
        $dishes = Dish::all()->sortBy('created_at');
        $category = Category::all();
        $x = null;
        $y = null;
        $ris = [];

        $orders->map(function ($order) {
            $order->created_date = $order->created_at->format('d-m-Y');
            return $order;
        });
        $orders->map(function ($order) {
            return $order->dishes;
        });

        switch ($request->get('ascisse')) {
            case 1:
                $x = $orders->groupBy('created_date');
                switch($request->get('ordinate'))
                {
                    case 1:
                        /* Ritorna il numero degli ordini fatti in un giorno */
                        $x = $x->map(function ($orders) {
                            return count($orders);
                        });
                    break;
                    case 2:
                        //TODO incasso di quelle giornate
                        $x = $orders->groupBy('created_date');
                        $x = $x->map(function ($orders) {
                            $price = 0;
                            foreach ($orders as $value) {
                                foreach ($value->dishes as $dish) {
                                    $price = $price + ($dish->pivot->single_price) * $dish->pivot->quantity;
                                }
                            }
                            return $price / 100;
                        });
                    break;

                }
            break;
            case 2:
                $dishes->map(function ($dish) {
                    return $dish->category;
                });
                $y = $dishes->mapToGroups(function ($item, $key){
                    return [$item['category']->name => $item];
                });
                switch($request->get('ordinate'))
                {
                    case 1:
                        /* Ritorna il numero degli ordini fatti in una categoria*/
                        $x = $orders->map(function ($order){
                            return $order->dishes->map(function ($dish){
                                return $dish->category;
                            });
                        });
                        $value= collect([]);
                        foreach ($category as $cat){
                            $i = 0;
                            foreach ($orders as $order) {
                                foreach ($order->dishes as $dish) {
                                    if ($dish->category_id == $cat->id){
                                        $i += $dish->pivot->quantity;
                                    }
                                }
                            }
                            $value->put($cat->name, $i);
                        }
                        return $value;

                        $x = $y->map(function ($dish) {
                            return count($dish);
                        });
                    break;
                    case 2:
                        $temp = collect([]);
                        $price = 0;
                        $orders->map(function ($order) {
                            $order->created_date = $order->created_at->format('d-m-Y');
                            return $order;
                        });
                        $orders->map(function ($order) {
                            return $order->dishes;
                        });
                        foreach ($category as $cat){
                            $price = 0;
                            foreach ($orders as $order) {
                                foreach ($order->dishes as $dish) {
                                    if ($dish->category_id == $cat->id){
                                        $price = $price + ($dish->pivot->single_price) * $dish->pivot->quantity;
                                    }
                                }
                            }
                            $temp->put($cat->name, $price/100);
                        }
                        return $temp;
                    break;
                }
            break;
        }
        return $x;
    }
}
