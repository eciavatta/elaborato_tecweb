<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\UserJoined;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JoinController extends Controller
{
    public function showStepOne()
    {
        if (!Auth::check()) {
            return view('auth.join', ['step1' => true, 'step2' => false,'step3' => false]);
        } else if (!session()->has('stepThree')) {
            return redirect()->route('home');
        }
    }

    public function showStepTwo(Request $request)
    {
        if (Auth::check() and $request->session()->has('stepTwo')) {
            return view('auth.join', ['step1' => false, 'step2' => true,'step3' => false]);
        } else {
            return redirect()->route('login');
        }
    }

    public function showStepThree(Request $request)
    {
        if (Auth::check() and $request->session()->has('stepThree')) {
            return view('auth.join', ['step1' => false, 'step2' => false,'step3' => true]);
        } else {
            return redirect()->route('login');
        }
    }

    public function stepOne(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|string|max:50',
            'lastname' => 'required|string|max:50',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        event(new Registered($user));
        Auth::guard()->login($user);
        $user->notify(new UserJoined());
        session()->put('stepTwo', true);
        session()->put('name', $request->get('firstname'));

        /* Telegram sync */
        $telegram_token = str_random(22);
        session()->put('telegram_token', $telegram_token);
        cache()->put($telegram_token, $user, 60);   // 60 minutes

        return redirect()->route('join.customize');
    }

    public function stepTwo(Request $request)
    {
        if (!Auth::check() or !session()->has('stepTwo')) {
            return redirect()->route('login');
        }

        $this->validate($request, [
            'address' => 'nullable|string|max:256',
            'telephone' => 'nullable|string|max:20',
        ]);

        $user = Auth::user();
        $user->address = $request->get('address');
        $user->telephone = $request->get('telephone');
        $user->save();

        session()->forget('stepTwo');
        session()->put('stepThree', true);

        /* Telegram sync */
        $telegram_token = session()->pull('telegram_token');
        cache()->pull($telegram_token);

        return redirect()->route('join.ready');
    }

    public function stepThree(Request $request)
    {
        if (!Auth::check() or !session()->has('stepThree')) {
            return redirect()->route('login');
        }

        session()->forget('name');
        session()->forget('stepThree');

        if (session()->has('buying')) {
            return redirect()->route('order');
        } else {
            return redirect()->route('home');
        }
    }
}
