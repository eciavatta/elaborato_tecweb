<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile()
    {
        return view('account')->with(['user' => Auth::user()]);
    }

    public function showDeleteAccount()
    {
        return view('account.delete');
    }

    public function editProfile(Request $request)
    {
        if (!$request->has('update')) {
            return redirect()->route('home');
        }

        $user = Auth::user();

        switch ($request->get('update')) {
            case 'general_information':
                $this->validate($request, [
                    'firstname' => 'required|string|max:50',
                    'lastname' => 'required|string|max:50',
                ]);
                $user->firstname = $request->get('firstname');
                $user->lastname = $request->get('lastname');
                break;
            case 'additional_information':
                $this->validate($request, [
                    'address' => 'nullable|string|max:256',
                    'telephone' => 'nullable|string|max:20',
                ]);
                $user->address = $request->get('address');
                $user->telephone = $request->get('telephone');
                break;
            case 'change_email':
                $this->validate($request, [
                    'email' => 'required|confirmed|email|max:50|unique:users',
                ]);
                $user->email = $request->get('email');
                break;
            case 'change_password':
                $this->validate($request, [
                    'password' => 'required|confirmed|string|min:6',
                ]);
                $user->password = bcrypt($request->get('password'));
                break;
            default:
                return redirect()->route('home');
        }

        $user->save();
        return redirect()->back()->with('success', trans('account.update_success'));
    }

    public function deleteAccount(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string',
            'verify' => 'required|string',
        ]);

        $user = Auth::user();
        if (!Hash::check($request->get('password'), $user->password)) {
            return redirect()->back()->withErrors(['password' => trans('account.wrong_password')]);
        } else if ($request->get('verify') !== trans('account.delete_command')) {
            return redirect()->back()->withErrors(['verify' => trans('account.wrong_command')]);
        }

        Auth::guard()->logout();
        $request->session()->invalidate();
        $user->delete();

        return redirect()->route('home');
    }
}
