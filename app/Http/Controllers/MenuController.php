<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dish;
use App\Http\Traits\CalculateTotalPrice;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    use CalculateTotalPrice;

    public function show()
    {
        session()->forget('buying');
        session()->forget('delivery');

        $categories = Category::all();
        return view('menu', ['categories' => $categories,
                             'totalPrice' => $this->getTotalPrice()]);
    }

    public function showCart()
    {
        return view('cart', ['totalPrice' => $this->getTotalPrice(), 'locked' => false]);
    }

    public function showSummary()
    {
        return view('cart.summary', ['totalPrice' => $this->getTotalPrice(), 'locked' => false]);
    }

    public function cartAdd(Request $request)
    {
        if (!session()->has('buying')) {
            $this->validate($request, [
                'dishId' => 'required|numeric',
            ]);

            $dish = Dish::findOrFail($request->dishId);

            $cart = session()->has('cart') ? session('cart') : [];
            $coll = collect($cart);
            if ($coll->has($dish->id)) {
                $quantity = $coll->get($dish->id)->get('quantity') + 1;
                $coll->get($dish->id)->put('quantity', $quantity);
            } else {
                $coll->put($dish->id, collect($dish->only(['name', 'price'])));
                $coll->get($dish->id)->put('quantity', 1);
            }

            session()->put('cart', $coll);
        }

        return view('cart.summary', ['totalPrice' => $this->getTotalPrice(), 'locked' => false]);
    }

    public function cartRemove(Request $request)
    {
        if (!session()->has('buying')) {
            $this->validate($request, [
                'dishId' => 'required|numeric',
            ]);

            if (!session()->has('cart')) {
                return view('cart.summary');
            }

            $coll = collect(session('cart'));
            $id = $request->get('dishId');
            if (!$coll->has($id)) {
                return view('cart.summary');
            }

            $quantity = $coll->get($id)->get('quantity');
            if ($quantity > 1) {
                $quantity = $quantity - 1;
                $coll->get($id)->put('quantity', $quantity);
            } else {
                $coll->pull($id);
            }

            session()->put('cart', $coll);
        }

        return view('cart.summary', ['totalPrice' => $this->getTotalPrice(), 'locked' => false]);
    }

    public function buy(Request $request)
    {
        $totalPrice = $this->getTotalPrice();

        if ($request->has('delivery') and $totalPrice >= 1500) {
            session()->put('buying', true);
            session()->put('delivery', true);
            return redirect()->route('order');
        }

        if ($request->has('pickup') and $totalPrice > 0) {
            session()->put('buying', true);
            session()->put('delivery', false);
            return redirect()->route('order');
        }

        return back();
    }
}
