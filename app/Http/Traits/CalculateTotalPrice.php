<?php

namespace App\Http\Traits;

trait CalculateTotalPrice
{
    private function getTotalPrice()
    {
        $totalPrice = 0;
        if (session()->has('cart')) {
            $totalPrice = session('cart')->reduce(function ($carry, $item) {
                return $carry + ($item['price'] * $item['quantity']);
            });
        }
        return $totalPrice;
    }
}
