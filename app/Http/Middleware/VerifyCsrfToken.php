<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    protected $except = [
        '544249062:AAFh4ttM2Ko7F2kC1n-Tih9xDeWVcLbtPJQ/webhook'
    ];
}
