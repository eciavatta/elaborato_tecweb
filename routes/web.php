<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', 'HomeController@index')->name('home');

/*ADMIN*/

Route::get('admin/dashboard', 'Admin\AdminController@index')->name('admin');
Route::any('admin/analytic', 'Admin\AnalyticController@index')->name('analyticPage');
Route::any('admin/order', 'Admin\OrderController@index')->name('orderPage');
Route::any('admin/users', 'Admin\UserController@index')->name('userPage');
Route::any('admin/edit', 'Admin\EditMenuController@index')->name('editPage');

Route::post('admin/edit/newDish', 'Admin\EditMenuController@newDish')->name('newDish');
Route::post('admin/edit/newCategory', 'Admin\EditMenuController@newCategory')->name('newCategory');
Route::post('admin/edit/delete', 'Admin\EditMenuController@deleteDish')->name('deleteDish');
Route::post('admin/edit/deleteCategory', 'Admin\EditMenuController@deleteCategory')->name('deleteCategory');
Route::post('admin/edit/update', 'Admin\EditMenuController@updatePrice')->name('updatePrice');

Route::any('admin/allOrder', 'Admin\OrderController@getAllOrder')->name('getAllOrder');
Route::any('admin/order/{n}', 'Admin\OrderController@getOrder')->name('getNOrder');
Route::post('admin/orderUpdate/', 'Admin\OrderController@updateOrder')->name('updateOrder');
Route::post('admin/getAnalytic', 'Admin\AnalyticController@getAnalytic')->name('getNAnalytic');

/* AUTH */

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Join Routes...
Route::get('join', 'Auth\JoinController@showStepOne')->name('join');
Route::post('join', 'Auth\JoinController@stepOne');
Route::get('join/customize', 'Auth\JoinController@showStepTwo')->name('join.customize');
Route::post('join/customize', 'Auth\JoinController@stepTwo');
Route::get('join/ready', 'Auth\JoinController@showStepThree')->name('join.ready');
Route::post('join/ready', 'Auth\JoinController@stepThree');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/* ACCOUNT */
Route::get('profile', 'AccountController@showProfile')->name('profile');
Route::post('profile', 'AccountController@editProfile');
Route::get('account/delete', 'AccountController@showDeleteAccount')->name('account.delete');
Route::post('account/delete', 'AccountController@deleteAccount');

/* MENU */
Route::get('menu', 'MenuController@show')->name('menu');

Route::get('cart', 'MenuController@showCart')->name('cart');
Route::put('cart/add', 'MenuController@cartAdd')->name('cart.add');
Route::delete('cart/remove', 'MenuController@cartRemove')->name('cart.remove');

Route::post('cart/buy', 'MenuController@buy')->name('cart.buy');

/* ORDER */
Route::get('order', 'OrderController@showStepOne')->name('order');
Route::post('order', 'OrderController@stepOne');
Route::get('order/payments', 'OrderController@showStepTwo')->name('order.payments');
Route::post('order/payments', 'OrderController@stepTwo');
Route::get('order/complete', 'OrderController@showStepThree')->name('order.complete');
Route::post('order/complete', 'OrderController@stepThree');

/* FOOTER */
Route::any('/contact-us', function () {
    return abort(404);
})->name('contact-us');

/* NEWSLETTER */

Route::post('/newsletter/subscribe', function () {
    return redirect()->route('home');
})->name('newsletter/subscribe');

Route::post('/newsletter/unsubscribe', function () {
    return redirect()->route('home');
})->name('newsletter/unsubscribe');

/* LEGAL */

Route::any('/legal/terms-of-use', function () {
    return abort(404);
})->name('terms-of-use');

Route::any('/legal/privacy-policy', function () {
    return abort(404);
})->name('privacy-policy');


/* TELEGRAM */

Route::post('544249062:AAFh4ttM2Ko7F2kC1n-Tih9xDeWVcLbtPJQ/webhook', function () {
    $updates = Telegram::commandsHandler(true);
    return 'ok';
});

Route::get('telegram/webhook', function () {
    if (config('app.debug')) {
        $updates = Telegram::commandsHandler(false);
        return 'ok';
    } else {
        return abort(404);
    }
});

Route::get('telegram/is-sync', function () {
    return [
        'result' => (boolean) Auth::user()->telegram_id
    ];
})->middleware('auth')->name('telegram.is-sync');
