<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->integer('dish_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('quantity');
            $table->integer('single_price');

            $table->primary(['dish_id', 'order_id']);

            $table->foreign('dish_id')
                  ->references('id')
                  ->on('dishes')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');

            $table->foreign('order_id')
                  ->references('id')
                  ->on('orders')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
