<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    $createdAt = $faker->dateTimeThisMonth;

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->companyEmail('studio.unibo.it'),
        'password' => $password ?: $password = bcrypt('secret'),
        'address' => $faker->streetAddress,
        'created_at' => $createdAt,
        'updated_at' => $createdAt,
        'deleted_at' => $faker->optional(0.1)->dateTimeBetween($createdAt, 'now'),
    ];
});
