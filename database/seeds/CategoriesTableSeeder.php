<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Pizze', 'Cassoni',];

        $i = 0;
        foreach ($categories as $category) {
            App\Category::create([
                'name' => $category,
                'order' => $i++,
            ]);
        }
    }
}
