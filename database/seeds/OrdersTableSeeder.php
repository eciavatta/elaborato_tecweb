<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    private const NUM_ORDERS = 200;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        foreach (range(1, OrdersTableSeeder::NUM_ORDERS) as $num) {
            $created_at = $faker->dateTimeThisMonth;

            $order = new App\Order([
                'address' => $faker->optional(0.5)->streetAddress,
                'time' => $faker->time('H:i'),
                'note' => $faker->optional(0.1)->text(200),
                'state' => $faker->randomElement(['received', 'processing', 'sent', 'delivered',]),
                'paid' => true,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);

            $user = App\User::inRandomOrder()->first();
            $user->orders()->save($order);
        }
    }
}
