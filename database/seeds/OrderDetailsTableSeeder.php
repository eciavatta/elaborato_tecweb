<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class OrderDetailsTableSeeder extends Seeder
{
    private const MAX_DISHES_PER_ORDER = 5;
    private const MAX_QUANTITY = 3;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        foreach (App\Order::all() as $order) {
            $dishes = App\Dish::inRandomOrder()
                              ->take($faker->numberBetween(1, OrderDetailsTableSeeder::MAX_DISHES_PER_ORDER))
                              ->get()
                              ->mapWithKeys(function ($dish) use ($faker) {
                                  return [
                                      $dish->id => [
                                          'quantity' => $faker->numberBetween(1, OrderDetailsTableSeeder::MAX_QUANTITY),
                                          'single_price' => $dish->price,
                                        ],
                                  ];
                              })
                              ->all();
            $order->dishes()->attach($dishes);
        }
    }
}
