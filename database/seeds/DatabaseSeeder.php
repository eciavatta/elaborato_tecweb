<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            DishesTableSeeder::class,
            OrdersTableSeeder::class,
            OrderDetailsTableSeeder::class,
            FavouritesTableSeeder::class,
        ]);
    }
}
