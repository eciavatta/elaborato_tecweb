<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class FavouritesTableSeeder extends Seeder
{
    private const NUM_USERS_WITH_FAVOURITES = 200;
    private const MAX_DISHES_PER_USER = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        foreach (App\User::inRandomOrder()->take(FavouritesTableSeeder::NUM_USERS_WITH_FAVOURITES)->get() as $user) {
            $dishes = App\Dish::inRandomOrder()
                              ->limit($faker->numberBetween(1, FavouritesTableSeeder::MAX_DISHES_PER_USER))
                              ->get()
                              ->map(function ($dish) use ($faker) {
                                  return $dish->id;
                              })
                              ->all();
            $user->favouriteDishes()->attach($dishes);
        }
    }
}
