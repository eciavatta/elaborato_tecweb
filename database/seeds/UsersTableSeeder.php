<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'firstname' => 'Emiliano',
            'lastname' => 'Ciavatta',
            'email' => 'emiliano.ciavatta@studio.unibo.it',
            'password' => bcrypt('eciavatta'),
            'address' => 'Via Sacchi, 3',
            'vendor' => true,
            'admin' => true,
        ]);

        App\User::create([
            'firstname' => 'Simone',
            'lastname' => 'Golinucci',
            'email' => 'simone.golinucci@studio.unibo.it',
            'password' => bcrypt('sgolinucci'),
            'address' => 'Via Sacchi, 3',
            'vendor' => true,
            'admin' => true,
        ]);

        App\User::create([
            'firstname' => 'Only',
            'lastname' => 'Vendor',
            'email' => 'only.vendor@studio.unibo.it',
            'password' => bcrypt('ovendor'),
            'address' => 'Via Sacchi, 3',
            'vendor' => true,
        ]);

        App\User::create([
            'firstname' => 'Only',
            'lastname' => 'Admin',
            'email' => 'only.admin@studio.unibo.it',
            'password' => bcrypt('oadmin'),
            'address' => 'Via Sacchi, 3',
            'admin' => true,
        ]);

        factory(App\User::class, 50)->create();
    }
}
