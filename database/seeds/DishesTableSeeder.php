<?php

use Illuminate\Database\Seeder;

class DishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'Pizze' => [
                [
                    'name' => 'Bianca / Focaccia',
                    'description' => 'Pizza con olio d\'oliva, rosmarino e sale',
                    'price' => 400,
                    'order' => 1,
                ],
                [
                    'name' => 'Margherita',
                    'description' => 'Pizza con pomodoro, mozzarella e basilico',
                    'price' => 500,
                    'order' => 2,
                ],
                [
                    'name' => 'Marinara',
                    'description' => 'Pizza con pomodoro, aglio e origano',
                    'price' => 450,
                    'order' => 3,
                ],
                [
                    'name' => 'Diavola',
                    'description' => 'Pizza con pomodoro, mozzarella e salame piccante',
                    'price' => 550,
                    'order' => 4,
                ],
                [
                    'name' => 'Boscaiola',
                    'description' => 'Pizza con pomodoro, mozzarella, salsiccia e funghi',
                    'price' => 650,
                    'order' => 5,
                ],
            ],
            'Cassoni' => [
                [
                    'name' => 'Rosso',
                    'description' => 'Cassone con pomodoro e mozzarella',
                    'price' => 300,
                    'order' => 1,
                ],
                [
                    'name' => 'Verde',
                    'description' => 'Cassone con erbe',
                    'price' => 300,
                    'order' => 2,
                ],
                [
                    'name' => 'Giallo',
                    'description' => 'Cassone con patate, salsiccia e mozzarella',
                    'price' => 350,
                    'order' => 3,
                ],
                [
                    'name' => 'Salame Piccante',
                    'description' => 'Cassone con pomodoro, mozzarella e salame piccante',
                    'price' => 350,
                    'order' => 4,
                ],
                [
                    'name' => 'Prosciutto Cotto e Funghi',
                    'description' => 'Cassone con mozzarella, prosciutto cotto e funghi',
                    'price' => 400,
                    'order' => 5,
                ],
            ],
        ];

        foreach ($array as $categoryName => $dishes) {
            $category = App\Category::where('name', $categoryName)->first();
            $category->dishes()->createMany($dishes);
        }
    }
}
